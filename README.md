Simple implementation of MVVM (component based variation) pattern via rxjs.

Documentation and code examples is coming soon, 
but here is a repo of a simple TODO app via binderTs: https://gitlab.com/nektoneket3/binderts-todo-app
and you can see result here https://stackblitz.com/edit/rxjs-szadpp?file=index.ts 

TODO:
1. Finish bootstrap functionality;
2. Improve bindable decorators;
3. Create type guards;
4. Finish Store class;
5. Add supporting for object property inside the template (object.prop) for component/directive output;
6. Refactor DI;
7. Refactor repeat directive;
8. Components inside table doesn't work (inside table can be placed only valid list of element and other are hoisted outside of table what is problem which need to be solved);
9. There is a bug with css animation in case of applying both data-bind-if and data-bind-class on the same element and data-bind-if value is an observable with debounceTime()
    
    Example:
    
    @input() editMode!: Input<EditMode>;
    
    @bindable(self => self.editMode.pipe(debounceTime(1000)))
    editModeAnimate!: Bindable<EditMode>;
    
    <div class="edit-section"
         data-bind-if="editMode"
         data-bind-class="{ 'insert-animation': 'editModeAnimate' }">
      content
    </div>
    
    <style>
      .edit-section.insert-animation {
        opacity: 1;
      }
      .edit-section {
        opacity: 0;
        transition: all .9s ease-in-out;
        z-index: 10;
      }
    </style>
