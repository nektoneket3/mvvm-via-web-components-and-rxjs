const path = require('path');
const gulp = require('gulp');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const alias = require('gulp-ts-alias');


const outputRoot = 'npm';

const paths = {
  entryPoint: ['src/**/*.ts', '!src/**/tests/**/*.ts'],
  dest: {
    module: `${outputRoot}/dist/module`,
    declarations: `${outputRoot}/dist/declarations`,
  },
};

const getTsProject = (settings) => ts.createProject('tsconfig.json', settings);

gulp.task('module', () => {
  const project = getTsProject();

  const tsResult = gulp.src(paths.entryPoint)
    .pipe(alias({ configuration: project.config }))
    .pipe(sourcemaps.init())
    .pipe(project());

  return tsResult.js
    .pipe(sourcemaps.write({ sourceRoot: file => path.relative(path.join(file.cwd, file.path), file.base) }))
    .pipe(gulp.dest(paths.dest.module));
});

gulp.task('declarations', () => {
  const project = getTsProject({ declaration: true });

  const tsResult = gulp.src(paths.entryPoint)
    .pipe(alias({ configuration: project.config }))
    .pipe(project());

  return tsResult.dts
    .pipe(gulp.dest(paths.dest.declarations));
});
