import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import nodeResolve from '@rollup/plugin-node-resolve';
import typeScript from '@rollup/plugin-typescript';
import { sizeSnapshot } from 'rollup-plugin-size-snapshot';
// import { terser } from 'rollup-plugin-terser'; // there is an error in minified bundle by terser, but not with uglify
import { uglify } from "rollup-plugin-uglify";
import visualizer from 'rollup-plugin-visualizer';
import copy from 'rollup-plugin-copy';

const packageJson = require('./package');


const getPlugins = (exclude = [], options = {}) => Object.entries({
  peerDepsExternal: peerDepsExternal(),
  nodeResolve: nodeResolve(),
  typeScript: typeScript(Object.assign({}, options)),
  sizeSnapshot: sizeSnapshot(),
  minify: uglify(),
  visualizer: visualizer()
}).reduce(
  (plugins, [pluginName, plugin]) => exclude.includes(pluginName) ? plugins : [...plugins, plugin],
  []
);

const outputRoot = 'npm';

const config = {
  input: 'src/index.ts',
  bundlesDir: `${outputRoot}/dist/bundles`,
  bundlesFile: 'index.umd.js',
  bundlesFileMin: 'index.umd.min.js',
  bundlesFormat: 'umd'
};

export default [
  {
    input: config.input,
    output: [
      {
        format: config.bundlesFormat,
        file: `${config.bundlesDir}/${config.bundlesFile}`,
        name: packageJson.name,
        // exports: 'named',
        sourcemap: true,
      }
    ],
    plugins: getPlugins(['minify'])
  },
  {
    input: config.input,
    output: [
      {
        format: config.bundlesFormat,
        file: `${config.bundlesDir}/${config.bundlesFileMin}`,
        name: packageJson.name,
        // exports: 'named',
        sourcemap: true,
      },
    ],
    plugins: [...getPlugins(), copy({
      targets: [
        { src: './package.json', dest: outputRoot },
      ]
    })]
  },
  // {
  //   input: config.input,
  //   output: {
  //     file: 'dist/app.js',
  //     format: 'cjs'
  //   },
  //   plugins: [
  //     copy({
  //       targets: [
  //         { src: 'src/index.html', dest: 'dist/public' },
  //         { src: ['assets/fonts/arial.woff', 'assets/fonts/arial.woff2'], dest: 'dist/public/fonts' },
  //         { src: 'assets/images/**/*', dest: 'dist/public/images' }
  //       ]
  //     })
  //   ]
  // }
];
