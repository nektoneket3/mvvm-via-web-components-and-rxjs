import { AppParams } from './types';
import { ComponentCtor } from '~/component';
import { ComponentContainer, ContainerFactory, IsStable } from '~/view';
import { componentMetadata, ComponentMetadata } from '~/metadata';
import { Injector, DependencyCtors, SharedDependencies, STORE } from '~/di';
import { createStore } from '~/store';


let app: any;
let injector: Injector;

export function bootstrapApp({ root, components, services, store: storeParams }: AppParams) {
  setInjector(components, services, storeParams);
  setRootContainer(root);
}

function setRootContainer(rootComponentCtor: ComponentCtor) {
  const { componentMeta: { name: mainName } }: ComponentMetadata = componentMetadata(rootComponentCtor).get();

  const rootElement = document.getElementsByTagName(mainName)[0] as HTMLElement;
  const rootElementCloned = rootElement.cloneNode(true) as HTMLElement;

  const rootComponentParams = undefined; // {};
  const containerFactory = new ContainerFactory(injector);

  // TODO case if into rootComponent passed data. Now it is hardcoded {} obj.
  if (rootComponentParams) {
    // app = containerFactory.createViewContainer(rootElementCloned, rootComponentParams);
  } else { // TODO case if into rootComponent nothing passed data
    // app = containerFactory.createComponentContainer(rootElementCloned, null);
    app = containerFactory.create(rootElementCloned, { component: {} }, null);
  }

  app.onInit().subscribe(() => {
    rootElement.replaceWith(rootElementCloned);
    app.onViewConnected(rootElement);
  });
}

function setInjector (components: ComponentCtor[], services: any, storeParams: any) { // TODO remove any
  const mappedComponents = components.reduce((acc: DependencyCtors, componentCtor: ComponentCtor) => {
    const { componentMeta } = componentMetadata(componentCtor).get();
    checkMeta(componentMeta, componentCtor);
    return { ...acc, [componentMeta.name]: componentCtor };
  }, {});

  const dependencyCtors: DependencyCtors = {
    ...services,
    ...mappedComponents
  };

  const sharedDependencies: SharedDependencies = storeParams
    ? { [STORE]: createStore(storeParams) }
    : {};

  injector = new Injector({ ...dependencyCtors }, { ...sharedDependencies });
}

function checkMeta(meta: ComponentMetadata['componentMeta'], componentCtor: ComponentCtor): void | never {
  if (!meta) {
    throw new Error(`${componentCtor.name} was not decorated with @component()`);
  }
}
// function service<T, R extends T>(service: R): R {
//   return service;
// }
