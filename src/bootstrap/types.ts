import { ComponentCtor } from '../component';
import { StoreParams } from '../store';
import { DependencyCtors } from '../di';


export interface AppParams {
  root: ComponentCtor,
  components: ComponentCtor[];
  services?: DependencyCtors,
  store?: StoreParams
}
