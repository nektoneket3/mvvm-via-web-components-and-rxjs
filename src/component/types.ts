import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';

import { ClassCtor, ClassInstance, OnInit, OnDestroy, OnViewConnected } from '~/types';


export type ComponentCtor = ClassCtor<Component>;
export type Component = ClassInstance & Partial<OnInit> & Partial<OnDestroy> & Partial<OnViewConnected>;
export type Input<T> = ReplaySubject<T> | BehaviorSubject<T>;
export type Output<T> = Subject<T>;
