import { paramIsValue } from './type-guards';
import { propertyKeyIsString } from '../type-guards';
import { ClassCtor } from '../types';
import { classMetadata, Metadata, ClassMetadata, BindableMetadata, FromValue, FromCallback, SubscribeCallback } from '../metadata';


class Bindable<T> {

  constructor(
    private metadata: Metadata<ClassMetadata>
  ) {}

  bindable(): PropertyDecorator;
  bindable<R>(valueOrCallback: FromValue<R> | FromCallback<T, R>): PropertyDecorator;
  bindable<R>(valueOrCallback: FromValue<R> | FromCallback<T, R>, subscribeCallback: SubscribeCallback<T, R>): PropertyDecorator;
  bindable<R>(param?: FromValue<R> | FromCallback<T, R>, subscribeCallback?: SubscribeCallback<T, R>): PropertyDecorator {
    return ({ constructor }: any, propertyKey: string | symbol): void => {
      if (propertyKeyIsString(propertyKey)) {
        const metadata: BindableMetadata = {
          ...this.getBindableProps(constructor)[propertyKey],
          bindable: true,
          source: null
        };

        if (typeof param === 'undefined') {
          metadata.source = null;
        } else if (paramIsValue(param)) {
          metadata.source = { fromValue: param };
        } else {
          metadata.source = { fromCallback: param };
        }

        if (subscribeCallback) {
          metadata.subscribe = this.getSubscribeCallback(metadata, propertyKey, subscribeCallback);
        }

        this.updateMetadata(propertyKey, metadata, constructor);
      }
    };
  }

  subscribe<I>(subscribeCallback: SubscribeCallback<T, I>): PropertyDecorator {
    return ({ constructor }: any, propertyKey: string | symbol): void => {
      if (propertyKeyIsString(propertyKey)) {
        let bindableMetadata: BindableMetadata | void = this.getBindableProps(constructor)[propertyKey];

        bindableMetadata = bindableMetadata && bindableMetadata.bindable
          ? {
            ...bindableMetadata,
            subscribe: this.getSubscribeCallback(bindableMetadata, propertyKey, subscribeCallback)
          }
          : {
            bindable: false,
            source: null,
            subscribe: subscribeCallback
          };

        this.updateMetadata(propertyKey, bindableMetadata, constructor);
      }
    };
  }

  subscribedBindable<I>(subscribeCallback: SubscribeCallback<T, I>): PropertyDecorator;
  subscribedBindable<I extends [I[0], I[1]]>(withLatestCallback: FromCallback<T, I[1]>, subscribeCallback: SubscribeCallback<T, I>): PropertyDecorator;
  subscribedBindable<I extends [I[0], I[1]]>(callback1: FromCallback<T, I[1]> | SubscribeCallback<T, I>, callback2?: SubscribeCallback<T, I>): PropertyDecorator {
    const argumentsLength = arguments.length;
    return ({ constructor }: any, propertyKey: string | symbol): void => {

      if (propertyKeyIsString(propertyKey)) {
        let bindableMetadata: BindableMetadata | void = this.getBindableProps(constructor)[propertyKey];

        if (bindableMetadata?.hasOwnProperty('bindable')) {
          throw new Error('subscribedBindable() used with bindable() or subscribe() at the same time.')
        }

        bindableMetadata = {
          bindable: true,
          source: null,
        };

        switch (argumentsLength) {
          case 1:
            bindableMetadata.subscribe = this.getSubscribeCallback(bindableMetadata, propertyKey, callback1 as SubscribeCallback<T, I>);
            break;
          case 2:
            bindableMetadata.subscribe = this.getSubscribeCallback(bindableMetadata, propertyKey, callback2 as SubscribeCallback<T, I>);
            bindableMetadata.withLatest = callback1 as FromCallback<T, I[1]>;
            break;
          default:
            throw new Error('Unexpected amount of callbacks');
        }

        this.updateMetadata(propertyKey, bindableMetadata, constructor);
      }
    };
  }

  private getBindableProps(ctor: ClassCtor): NonNullable<ClassMetadata['bindableProperties']> {
    return this.metadata.getPropOwnProps('bindableProperties', ctor) || {};
  }

  private updateMetadata(propertyKey: string, bindableMetadata: BindableMetadata, ctor: ClassCtor): void {
    this.metadata.deepUpdate({ bindableProperties: { [propertyKey]: bindableMetadata } }, ctor);
  }

  private getSubscribeCallback<R>(meta: BindableMetadata, key: string, callback: SubscribeCallback<T, R>): SubscribeCallback<T, R> | never {
    if (meta.subscribe) {
      throw new Error(`Subscribe callback for ${key} already exists.`);
    }
    return callback;
  }
}

export function getBindableDecorators<T>(): Pick<Bindable<T>, 'bindable' | 'subscribe' | 'subscribedBindable'> {
  const decorators = new Bindable<T>(classMetadata());

  return {
    bindable(...args: any) {
      return decorators.bindable.apply(decorators, args)
    },
    subscribe(...args: any) {
      return decorators.subscribe.apply(decorators, args)
    },
    subscribedBindable(...args: any) {
      return decorators.subscribedBindable.apply(decorators, args)
    },
  };
}
