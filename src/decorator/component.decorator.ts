import { ComponentCtor } from '../component';
import { ComponentMetadata, componentMetadata } from '../metadata';


type ComponentDecoratorParams = ComponentMetadata['componentMeta'] & Pick<ComponentMetadata, 'dependencies'>;

class DecoratedClassNameError extends Error {

  constructor(className: string) {
    super(`${className} name is not valid`);

    this.name = 'DecoratedClassNameError';
  }
}

export function component(params: ComponentDecoratorParams) {

  return (componentCtor: ComponentCtor): void => {

    if (!params.template) {
      throw new Error(`${componentCtor.name} template is not valid`);
    }
    if (!params.name) {
      throw new DecoratedClassNameError(componentCtor.name);
    }

    const metadata = componentMetadata(componentCtor);

    if (metadata.getPropOwnProps('dependencies') && params.dependencies) {
      throw new Error(`${componentCtor.name}: dependencies already set.`);
    }

    metadata.update({
      componentMeta: {
        name: params.name,
        template: params.template,
        style: params.style,
      },
      ...( params.dependencies ? { dependencies: params.dependencies } : {} )
    });
  };
}

// TODO finish implementing directive decorator
// type DirectiveDecoratorParams = DirectiveMetadata['directiveMeta'] & Pick<DirectiveMetadata, 'dependencies'>;
//
// export function directive(params: DirectiveDecoratorParams) {
//
//   return (directiveCtor: ClassCtor<Directive>): void => {
//
//     if (!params.name) {
//       throw new DecoratedClassNameError(directiveCtor.name);
//     }
//
//     const metadata = directiveMetadata(directiveCtor);
//
//     if (metadata.getPropOwnProps('dependencies') && params.dependencies) {
//       throw new Error(`${directiveCtor.name}: dependencies already set.`);
//     }
//
//     metadata.update({
//       directiveMeta: {
//         name: params.name,
//       },
//       ...( params.dependencies ? { dependencies: params.dependencies } : {} )
//     });
//   };
// }
