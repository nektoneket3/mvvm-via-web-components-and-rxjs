import { classMetadata, InputMetadata } from '../metadata';
import { propertyKeyIsString } from '../type-guards';


type Name = InputMetadata<any>['publicPropertyName'];
type Default<T> = InputMetadata<T>['defaultValue'];

interface InputDecoratorParams<T> {
  name?: Name;
  default?: Default<T>;
}

export function input<T>(params?: InputDecoratorParams<T> | T): PropertyDecorator {
  return ({ constructor }: any, propertyKey: string | symbol) => {

    if (propertyKeyIsString(propertyKey)) {

      let meta: InputMetadata<T> = {
        propertyName: propertyKey,
        publicPropertyName: propertyKey,
      };

      if (paramsIsObjectLike(params)) {
        meta.publicPropertyName = params.name || meta.publicPropertyName;
        if (typeof params.default !== 'undefined') {
          meta.defaultValue = params.default;
        }
      } else if (typeof params !== 'undefined') {
        meta.defaultValue = params;
      }

      classMetadata(constructor).deepUpdate({ inputs: { [propertyKey]: meta } });
    }
  };
}

function paramsIsObjectLike(value?: InputDecoratorParams<any> | any): value is InputDecoratorParams<any> {
  return value?.hasOwnProperty('name') || value?.hasOwnProperty('default');
}
