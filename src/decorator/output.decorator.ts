import { classMetadata, OutputMetadata } from '../metadata';
import { propertyKeyIsString } from '../type-guards';


type Name = OutputMetadata['publicPropertyName'];

export function output(name?: Name): PropertyDecorator {
  return ({ constructor }: any, propertyKey: string | symbol) => {

    if (propertyKeyIsString(propertyKey)) {

      const meta: OutputMetadata = {
        propertyName: propertyKey,
        publicPropertyName: name || propertyKey
      };

      classMetadata(constructor).deepUpdate({ outputs: { [propertyKey]: meta } });
    }
  };
}
