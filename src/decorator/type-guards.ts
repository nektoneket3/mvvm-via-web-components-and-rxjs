import { BindableSource, BindableFromCallback, BindableFromValue, FromValue, FromCallback } from '../metadata';


export function sourceIsValue<T = any, R = any>(value: BindableSource<T, R>): value is BindableFromValue<R> {
  return value!.hasOwnProperty('fromValue');
}

export function sourceIsCallback<T = any, R = any>(value: BindableSource<T, R>): value is BindableFromCallback<T, R> {
  return value!.hasOwnProperty('fromCallback');
}

export function paramIsValue<T = any, R = any>(param: FromValue<R> | FromCallback<T, R>): param is FromValue<R> {
  return typeof param !== 'function';
}

export function paramIsCallback<T = any, R = any>(param: FromValue<R> | FromCallback<T, R>): param is FromCallback<T, R> {
  return typeof param === 'function';
}
