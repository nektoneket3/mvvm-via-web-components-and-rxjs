import { Subject, Subscription } from 'rxjs';


export type Bindable<T> = Subject<T>;
export type BindableSubscriptions = Record<string, Subscription>;
