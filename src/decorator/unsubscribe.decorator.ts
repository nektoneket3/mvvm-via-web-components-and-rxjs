import { classMetadata } from '../metadata';
import { propertyKeyIsString } from '../type-guards';


export function unsubscribe(params?: {}): PropertyDecorator {
  return ({ constructor }: any, propertyKey: string | symbol): void => {

    if (propertyKeyIsString(propertyKey)) {
      classMetadata(constructor).deepUpdate({ subscriptions: { [propertyKey]: propertyKey } });
    }
  };
}
