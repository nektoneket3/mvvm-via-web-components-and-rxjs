import { ClassCtor } from '../types';
import { classMetadata, ClassDependencyList } from '../metadata';


export function inject(...dependencyNames: ClassDependencyList) {

  return (classCtor: ClassCtor): void => {

    const metadata = classMetadata(classCtor);

    if (dependencyNames.length) {

      if (metadata.getPropOwnProps('dependencies')) {
        throw new Error(`${classCtor.name}: dependencies already set.`); // TODO handle duplicating in component decorator
      }

      metadata.update({ dependencies: dependencyNames });
    }
  };
}
