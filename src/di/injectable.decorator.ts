import { ClassCtor } from '../types';
import { classMetadata, ClassInjectable } from '../metadata';


export function injectable(params: ClassInjectable = { shared: false }) {

  return (classCtor: ClassCtor): void => {

    classMetadata(classCtor).update({ injectable: params });
  };
}
