import { ClassCtor } from '../types';
import { classMetadata, DependencyName, ClassDependencyList } from '../metadata';
import { DependencyCtor, DependencyCtors, SharedDependencies, DependencyInstance } from './types';
import { INJECTOR } from './tokens';


export class Injector {

  constructor(
    private dependencyCtors: DependencyCtors,
    protected sharedDependencies: SharedDependencies // instances
  ) {}

  getConstructor(name: DependencyName): DependencyCtor {
    return this.dependencyCtors[name];
  }

  getDependencies(classCtor: ClassCtor): DependencyInstance[] {
    const { dependencies } = classMetadata(classCtor).get();
    return this.constructDependencies(classCtor, dependencies);
  }

  protected constructDependencies(classCtor: ClassCtor, dependencyNames?: ClassDependencyList): DependencyInstance[] | never {

    if (!dependencyNames) {
      return [];
    }

    return dependencyNames.map((dependencyName) => {
      if (dependencyName === INJECTOR) {
        return this;
      }

      if (this.sharedDependencies[dependencyName]) {
        return this.sharedDependencies[dependencyName];
      }

      const dependencyCtor = this.getConstructor(dependencyName);

      if (dependencyCtor) {
        const { dependencies: dependenciesForDependency, injectable: { shared } = {} } = classMetadata(dependencyCtor).get();

        const dependency = dependenciesForDependency
          ? new dependencyCtor(...this.getDependencies(dependencyCtor))
          : new dependencyCtor();

        if (shared) {
          this.sharedDependencies[dependencyName] = dependency;
        }

        return dependency;
      }
      throw new Error(`Cannot resolve dependency ${dependencyName} for class ${classCtor.name}.`);
    });
  }
}

export class LocalInjector extends Injector {

  protected constructDependencies(classCtor: ClassCtor): DependencyInstance[] {

    const { dependencies = [] } = classMetadata(classCtor).get();

    return dependencies.map(dependencyName => this.sharedDependencies[dependencyName] || dependencyName);
  }

}
