export const VIEW = 'view'; // TODO probably must be a symbol type
export const VIEW_UPDATED = 'viewUpdated';
export const COMPONENT = 'component';
export const INJECTOR = 'injector';
export const STORE = 'store';
export const SUBSCRIPTIONS = 'subscriptions';
