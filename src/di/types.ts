import { ClassCtor, ClassInstance } from '../types';
import { Store } from '../store';
import { DependencyName } from '../metadata';


export type DependencyCtor = ClassCtor;
export type DependencyCtors = Record<DependencyName, DependencyCtor>;

export type DependencyInstance = ClassInstance;
type DependencyInstances = Record<DependencyName, DependencyInstance>;

interface StoreDependency<T = any> {
  Store?: Store<T>
}

export type SharedDependencies = DependencyInstances & StoreDependency;
