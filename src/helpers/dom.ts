type TemplateString = string;
type StyleString = string;

export function getTemplateElement(templateString: TemplateString, styleString: StyleString = ''): HTMLTemplateElement {
  const templateElement = document.createElement('template');
  templateElement.innerHTML = `${styleString}${templateString}`.trim();
  return templateElement;
}

export function getViewTemplate(templateString: TemplateString, styleString?: StyleString): HTMLElement { // TODO check this function return type
  return getTemplateElement(templateString, styleString).content.cloneNode(true) as HTMLElement;
}

export function getElementFromString(templateString: TemplateString, styleString?: StyleString): HTMLElement {
  return getViewTemplate(templateString, styleString).firstChild as HTMLElement;
}

export function getCustomElement(name: string): HTMLElement { // TODO check this function return type
  return getViewTemplate(`<${name}></${name}>`); // TODO probably use getElementFromString here
}

export function attachShadowRoot(element: HTMLElement, rootTemplate: HTMLElement): void {
  let shadowRoot: ShadowRoot;
  shadowRoot = element.attachShadow({ mode: 'open' });
  shadowRoot.appendChild(rootTemplate);
}

export function createAndAttachShadowRoot( // TODO check this function return type
  element: HTMLElement,
  templateString: TemplateString,
  styleString?: StyleString
): void {
  attachShadowRoot(element, getViewTemplate(templateString, styleString));
}

export function hasElementChildren(element: HTMLElement): boolean {
  return !!element.children.length;
}
