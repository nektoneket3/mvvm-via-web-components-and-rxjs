export {
  getTemplateElement,
  getViewTemplate,
  attachShadowRoot,
  createAndAttachShadowRoot,
  hasElementChildren,
  getCustomElement,
  getElementFromString
} from './dom';
