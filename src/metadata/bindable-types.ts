import { Observable } from 'rxjs';


export type FromValue<T> = T;
export type FromCallback<T, R> = (self: T) => Observable<R>;
export type SubscribeCallback<T, V> = (value: V, self: T) => void;

export interface BindableFromValue<T> {
  fromValue: FromValue<T>;
}
export interface BindableFromCallback<T, R> {
  fromCallback: FromCallback<T, R>;
}

export type BindableSource<T, R> = BindableFromValue<R> | BindableFromCallback<T, R> | null;

export type PropertyKey = string;
type TwoWay = boolean;
type IsBindable = boolean;

export interface BindableMetadata<T = any, R = any> {
  bindable: IsBindable;
  source: BindableSource<T, R>;
  subscribe?: SubscribeCallback<T, R>;
  withLatest?: FromCallback<T, R>;
  twoWay?: TwoWay; // TODO unused and probably will be removed in the next release
}

export type BindableProperties = Record<PropertyKey, BindableMetadata>;
