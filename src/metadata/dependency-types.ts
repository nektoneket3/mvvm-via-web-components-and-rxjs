export type DependencyName = string;
export type ClassDependencyList = DependencyName[];


export interface ClassInjectable {
  shared: boolean;
}
