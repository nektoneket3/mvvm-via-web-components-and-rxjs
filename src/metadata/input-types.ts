export interface InputMetadata<T> {
  propertyName: string;
  publicPropertyName: string;
  defaultValue?: T;
}

export interface ClassInputMetadata {
  [key: string]: InputMetadata<any>;
}

export interface OutputMetadata {
  propertyName: string;
  publicPropertyName: string;
}

export interface ClassOutputMetadata {
  [key: string]: OutputMetadata;
}

