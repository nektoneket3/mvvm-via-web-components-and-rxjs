import 'reflect-metadata';

import { ClassInstance } from '../types';
import { ClassMetadata, ComponentMetadata } from './types';


type MetadataKey = string;
type ClassName = string;
type MetaPropName<T> = keyof T;
type MetaPropValue<T> = T[MetaPropName<T>];
type MetadataContainer<T> = Record<MetaPropName<T>, Record<ClassName, MetaPropContainer<MetaPropValue<T>>>>;

export class Metadata<T extends ClassInstance> {

  constructor(
    private target?: any,
    private metadataKey: MetadataKey = 'metadata'
  ) {}
  /**
   * Converts metadata from MetadataContainer structure to the initial structure and returns it;
   */
  get(target = this.target): T {
    return Object.entries(this.getMetadataContainer(target)).reduce(
      (acc: T, [key, value]): T => {
        const props = (value[target.name] || value[Object.getPrototypeOf(target).name]).getProps();
        return { ...acc, ...(props ? { [key]: props } : {}) }
      },
      {} as T
    );
  }
  /**
   * Returns only own properties (without inherited) of metadata property.
   */
  getPropOwnProps<K extends keyof T>(propKey: K, target = this.target): T[K] | void {
    return this.getMetadataContainer(target)[propKey]?.[target.name]?.getOwnProps();
  }
  /**
   * Returns metadata wrapped in a MetadataContainer;
   */
  getAllData(target = this.target): MetadataContainer<T> {
    return this.getMetadataContainer(target);
  }
  /**
   *
   */
  update(value: T, target = this.target): void {
    this.setMetadataContainer({
      ...this.getMetadataContainer(target),
      ...this.createMetadataContainer(value, target)
    }, target);
  }

  deepUpdate(value: T, target = this.target): void {
    const metadata: MetadataContainer<T> = this.getMetadataContainer(target);
    this.deepAssign(metadata, this.createMetadataContainer(value, target));
    this.setMetadataContainer(metadata, target); // TODO do I need to call it?
  }

  private getMetadataContainer(target = this.target): MetadataContainer<T> {
    return Reflect.getMetadata(this.metadataKey, target) || {};
  }

  private createMetadataContainer(value: T, target = this.target): MetadataContainer<T> {
    const metadata = this.getMetadataContainer(target);
    return Object.entries(value).reduce(
      (acc: MetadataContainer<T>, [propKey, propValue]: [MetaPropName<T>, MetaPropValue<T>]): MetadataContainer<T> => {
        acc[propKey] = {
          [target.name]: new MetaPropContainer<MetaPropValue<T>>(
            propValue,
            metadata[propKey]?.[Object.getPrototypeOf(target).name] || null,
          )
        };
        return acc;
      },
      {} as MetadataContainer<T>
    );
  }

  private setMetadataContainer(value: MetadataContainer<T>, target = this.target): void {
    Reflect.defineMetadata(this.metadataKey, value, target);
  }

  private deepAssign(target: ClassInstance, src: ClassInstance): void {
    const isObject = (obj: any): boolean => Array.isArray(obj) ? false : obj === Object(obj);
    // const isObject = (x) => typeof x === "object" ? x !== null : typeof x === "function";
    Object.entries(src).forEach(([key, _value]) => {
      if (isObject(_value)) {
        if (!target[key]) {
          target[key] = _value;
        }
        this.deepAssign(target[key], _value);
      } else {
        target[key] = _value;
      }
    })
  };
}

class MetaPropContainer<T extends ClassInstance | Array<any>>{

  constructor(
    private props: T,
    private parent: MetaPropContainer<T> | null,
  ) {}
  /**
   * Returns object with own properties + inherited from parent properties.
   */
  getProps(): T {
    if (Array.isArray(this.props)) {
      return this.props || this.parent?.getProps();
    }
    return Object.entries(this.parent?.getProps() || {}).reduce(
      (acc: T, [key, value]) => !acc.hasOwnProperty(key) ? { ...acc, [key]: value } : acc,
      { ...this.props } as T
    );
  }

  getOwnProps(): T {
    return this.props;
  }
}

export function classMetadata(target?: any): Metadata<ClassMetadata> {
  return new Metadata<ClassMetadata>(target);
}

export function componentMetadata(target?: any): Metadata<ComponentMetadata> {
  return new Metadata<ComponentMetadata>(target);
}

// TODO finish implementing directive decorator
// export function directiveMetadata(target?: any): Metadata<DirectiveMetadata> {
//   return new Metadata<DirectiveMetadata>(target);
// }
