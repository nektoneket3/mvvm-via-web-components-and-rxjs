import { BindableProperties } from './bindable-types';
import { ClassDependencyList, ClassInjectable } from './dependency-types';
import { ClassInputMetadata, ClassOutputMetadata } from './input-types';


export type Subscription = string;
export type Subscriptions = Record<string, Subscription>;

interface ArrayOrObject {
  [key: string]: Record<string, any> | Array<any> | void;
}

export interface ClassMetadata extends ArrayOrObject {
  injectable?: ClassInjectable;
  dependencies?: ClassDependencyList;
  bindableProperties?: BindableProperties;
  inputs?: ClassInputMetadata;
  outputs?: ClassOutputMetadata;
  subscriptions?: Subscriptions;
}

export interface ComponentMetadata extends ClassMetadata {
  componentMeta: {
    name: string;
    template: string;
    style?: string;
  }
}

// TODO finish implementing directive decorator
// export interface DirectiveMetadata extends  ClassMetadata {
//   directiveMeta: {
//     name: string;
//   }
// }
