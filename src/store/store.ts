import { of } from 'rxjs';

import { StoreParams, Mutations, MutationName } from './types';


export class Store<S> {

  dispatch = {
    mutation: <T>(mutationName: MutationName, payload?: T) => {
      if (this.mutations[mutationName]) {
        this.mutations[mutationName](this.state, of(payload));
      }
    }
  };

  constructor(
    public state: S,
    public mutations: Mutations<S>
  ) {}

  dispatchMutation<T>(mutationName: MutationName, payload?: T) {
    if (this.mutations[mutationName]) {
      this.mutations[mutationName](this.state, payload);
    }
  }
}

export function createStore<T>({ state, mutations = {} }: StoreParams<T>): Store<T> {
  return new Store<T>(state, mutations);
}
