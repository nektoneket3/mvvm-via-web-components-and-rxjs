type Mutation<S> = (state: S, payload: any) => void;

export type Mutations<S> = Record<string, Mutation<S>>

export type MutationName = string;

export interface StoreParams<T = any> {
  state: T,
  mutations?: Mutations<T>
}
