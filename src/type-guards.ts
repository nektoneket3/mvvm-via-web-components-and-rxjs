import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';

import { Bindable } from '~/decorator';
import { Input, Output } from '~/component';


export function propertyKeyIsString(value: string | Symbol): value is string | never {
  if (typeof value === 'string') {
    return true;
  }
  throw new Error('propertyKey is not a string');
}

export function isInput<T>(value?: Input<T>): value is Input<T> {
  return value instanceof BehaviorSubject || value instanceof ReplaySubject;
}

export function isOutput<T>(value?: Output<T>): value is Output<T> {
  return value instanceof Subject;
}

export function isBindable<T>(value?: Bindable<T>): value is Bindable<T> {
  return value instanceof Subject;
}

export function isBindableOrCallback<T>(value?: Bindable<T> | Function): value is Bindable<T> {
  return value instanceof Subject;
}
