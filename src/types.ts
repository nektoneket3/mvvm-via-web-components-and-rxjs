import { Bindable } from '~/decorator';


export interface ClassCtor<T = {}> {
  new (...args: any[]): T
}

export type ClassInstance = Record<string, any>;

export interface OnInit {
  onInit(): void
}

export interface OnDestroy {
  onDestroy(): void
}

export interface OnViewConnected {
  onViewConnected: Bindable<void> | Function;
}
