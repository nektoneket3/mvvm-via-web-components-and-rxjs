import { Injector } from '~/di';
// import { ComponentMetadata, componentMetadata } from '../../metadata';
import { Component, ComponentCtor } from '~/component';
import { ClassCtor } from '~/types';
import { View } from '../types';
import { ParentContainer } from './types';
// import { isView } from '../type-guards';
// import { createAndAttachShadowRoot, getCustomElement } from '../../helpers';
import { Container } from './containers/container';
import { IfDirectiveViewContainer } from './containers/if-directive-view-container';
import { RepeatDirectiveViewContainer } from './containers/repeat-directive-view-container';
import { AttrDirectiveViewContainer } from './containers/attr-directive-view-container';
import { ComponentContainer } from './containers/component-container';
import { ViewContainer } from './containers/view-container';
// import { ComponentBindingParser } from '../directive/parsers/component-binding-parser';
import { BindingSource } from '../directive/parsers/types';


interface CreateContainer {
  create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): Container | null
}

export interface ContainerFactory {

  create(view: View, bindingSource: BindingSource, parentContainer: ParentContainer): Container
  /**
  * Finds component constructor by view.localName and insert into it created based on component metadata shadowDOM.
  */
  // createComponentContainer(view: View, parentContainer: ParentContainer): ComponentContainer;
  /**
  * Will create new element and insert into it created based on component metadata shadowDOM.
  */
  // createComponentContainer(componentCtor: ComponentCtor, parentContainer: ParentContainer): ComponentContainer;
  /**
  *
  */
  createViewContainer(view: View, component: Component, parentContainer: ParentContainer): ViewContainer;
}

export class ContainerFactory implements ContainerFactory {

  private readonly containers: (ClassCtor<Container> & CreateContainer)[] = [
    IfDirectiveViewContainer,
    RepeatDirectiveViewContainer,
    AttrDirectiveViewContainer,
    ComponentContainer,
    ViewContainer
  ];

  private possibleContainers: (ClassCtor<Container> & CreateContainer)[] = [...this.containers];

  constructor(
    private injector: Injector
  ) {}

  excludeIfContainer(): this {
    this.possibleContainers = this.possibleContainers.filter(container => container.name !== IfDirectiveViewContainer.name);
    return this;
  }

  excludeRepeatContainer(): this {
    this.possibleContainers = this.possibleContainers.filter(container => container.name !== RepeatDirectiveViewContainer.name);
    return this;
  }

  excludeAttrDirectiveContainer(): this {
    this.possibleContainers = this.possibleContainers.filter(container => container.name !== AttrDirectiveViewContainer.name);
    return this;
  }

  excludeComponentContainer(): this {
    this.possibleContainers = this.possibleContainers.filter(container => container.name !== ComponentContainer.name);
    return this;
  }

  allowAll(): this {
    this.possibleContainers = [...this.containers];
    return this;
  }

  create(view: View, bindingSource: BindingSource, parentContainer: ParentContainer): Container {
    for (const containerCtor of this.possibleContainers) {
      const container = containerCtor.create(view, bindingSource, parentContainer, new ContainerFactory(this.injector), this.injector);
      if (container) {
        return container;
      }
    }

    return ViewContainer.create(view, bindingSource, parentContainer, new ContainerFactory(this.injector), this.injector);
  }

  // createComponentContainer(param: View | ComponentCtor, parentContainer: ParentContainer): ComponentContainer {
  //   if (isView(param)) {
  //     const componentCtor: ComponentCtor = this.injector.getConstructor(param.localName);
  //     const { componentMeta: { template, style } }: ComponentMetadata = componentMetadata(componentCtor).get();
  //     createAndAttachShadowRoot(param, template, style);
  //
  //     return new ComponentContainer(param, componentCtor, parentContainer, new ContainerFactory(this.injector), this.injector, new ComponentBindingParser(param));
  //   }
  //
  //   const { componentMeta: { template, style, name } }: ComponentMetadata = componentMetadata(param).get();
  //   const view: View = getCustomElement(name); // TODO check it
  //   createAndAttachShadowRoot(view, template, style);
  //
  //   return new ComponentContainer(view, param, parentContainer, new ContainerFactory(this.injector), this.injector, new ComponentBindingParser(view));
  // }

  createViewContainer(view: View, component: Component, parentContainer: ParentContainer): ViewContainer {
    return new ViewContainer(view, { component }, parentContainer, new ContainerFactory(this.injector), this.injector);
  }
}
