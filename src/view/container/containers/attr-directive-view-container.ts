import { Injector } from '~/di';
import { DirectiveCtor, DirectiveCtorOrFactory, View } from '../../types';
import { DirectiveLocalDependencies, IsStable$, ParentContainer } from '../types';
import { Container } from './container';
import { ContainerFactory } from '../container-factory';
import { BindingSource } from '../../directive/parsers/types';
import { AttrDirectiveBindingParser } from '../../directive/parsers/attr-directive-binding-parser';
import { DirectiveViewContainer } from './directive-view-container';
import { attrBuiltInDirectives } from '../../directive/built-in-directives';
import { DirectiveBuilder } from '../meta-class-builder/meta-class-builder';


export class AttrDirectiveViewContainer extends DirectiveViewContainer {

  static create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): AttrDirectiveViewContainer | null {
    const directiveBindingParser: AttrDirectiveBindingParser = new AttrDirectiveBindingParser(view);

    return directiveBindingParser.viewContainsDirective()
      ? new AttrDirectiveViewContainer(view, bindingSource, parent, containerFactory, injector, directiveBindingParser)
      : null;
  }

  protected createDirectiveBuilders(localDependencies: DirectiveLocalDependencies): DirectiveBuilder[] {
    return Object.entries(attrBuiltInDirectives).reduce(
      (acc: DirectiveBuilder[], [directiveName, directiveCtorOrFactory]): DirectiveBuilder[] => {

        const getCtor = (ctorOrFactory: DirectiveCtorOrFactory, view: View): DirectiveCtor => {
          return typeof ctorOrFactory === 'function' ? ctorOrFactory : ctorOrFactory.factory(view);
        };

        return this.bindingParser.viewContainsDirective(directiveName)
          ? [...acc, new DirectiveBuilder(getCtor(directiveCtorOrFactory, localDependencies.view), this.injector, localDependencies, this.bindingParser)]
          : acc;
      },
      []
    );
  }

  protected onDirectivesSet(view: View): IsStable$ {
    return this.initChildContainers(view);
  }

  protected createChildContainer(view: View): Container {
    return this.containerFactory
      .excludeIfContainer()
      .excludeRepeatContainer()
      .excludeAttrDirectiveContainer()
      .create(view, this.bindingSource, this);
  }
}
