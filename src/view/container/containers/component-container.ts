import { Subscription } from 'rxjs';

import { Injector, VIEW } from '~/di';
import { Component, ComponentCtor } from '~/component';
import { classMetadata, componentMetadata, ComponentMetadata } from '~/metadata';
import { createAndAttachShadowRoot } from '~/helpers';
import { View } from '../../';
import { IsStable$, ParentContainer } from '../types';
import { ContainerFactory } from '../container-factory';
import { ComponentBuilder } from '../meta-class-builder/meta-class-builder';
import { Container } from './container';
import { BindingParser } from '../../directive/parsers/binding-parser';
import { BindingSource } from '../../directive/parsers/types';
import { ComponentBindingParser } from '../../directive/parsers/component-binding-parser';
import { PossibleView } from '~/view/types';
import { isBindableOrCallback } from '~/type-guards';


/**
 * ComponentContainer is responsible for creating component from passed component constructor,
 * creating component shadowDOM (from component metadata) and creating Container for each child in shadowDOM.
 */
export class ComponentContainer extends Container {

  private component!: Component;

  constructor(
    private view: View,
    protected bindingSource: BindingSource,
    parent: ParentContainer,
    private containerFactory: ContainerFactory,
    private injector: Injector,
    private bindingParser: BindingParser,
    private componentCtor: ComponentCtor,
  ) {
    super(parent);
  }

  static create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): ComponentContainer | null {
    const componentCtor: ComponentCtor = injector.getConstructor(view.localName);
    if (componentCtor) {
      const { componentMeta: { template, style } }: ComponentMetadata = componentMetadata(componentCtor).get();
      createAndAttachShadowRoot(view, template, style);
      return new ComponentContainer(view, bindingSource, parent, containerFactory, injector, new ComponentBindingParser(view), componentCtor);
    }
    return null;
  }

  onInit(): IsStable$ {
    this.setComponent();
    return this.initChildContainers(this.view.shadowRoot!.children);
  }

  onViewConnected(updatedView: PossibleView): void {
    isBindableOrCallback(this.component.onViewConnected)
      ? this.component.onViewConnected.next()
      : this.component.onViewConnected?.();
    super.onViewConnected(updatedView);
  }

  onDestroy(): void {
    this.componentUnsubscribeAll(); // TODO check if unsubscribe works good
    this.component.onDestroy?.();
    super.onDestroy();
  }

  protected createChildContainer(view: View): Container {
    return this.containerFactory.allowAll().create(view, { component: this.component }, this);
  }

  private setComponent(): void {
    const localDependencies = { [VIEW]: this.view };
    const componentBuilder = new ComponentBuilder(this.componentCtor, this.injector, localDependencies, this.bindingParser);

    componentBuilder
      .create()
      .setMetaProps()
      .setBindableSubscriptions();

    if (this.parent !== null) {
      componentBuilder
        .setInputSubscriptions(this.bindingSource)
        .setOutputSubscriptions(this.bindingSource);
    }
    this.subscriptions.add(componentBuilder.getSubscriptions());
    this.component = componentBuilder.getResult();
    this.component.onInit?.();
  }

  private componentUnsubscribeAll(): void {
    const { subscriptions = {} } = classMetadata(this.component.constructor).get();
    Object.values(subscriptions).forEach((subscription: string) => {
      if (this.component[subscription] instanceof Subscription) {
        this.component[subscription].unsubscribe();
      }
    });
  }
}
