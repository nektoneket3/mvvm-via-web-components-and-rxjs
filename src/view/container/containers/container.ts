import { forkJoin, of, Subscription } from 'rxjs';
import { mapTo } from 'rxjs/operators';

import { PossibleView } from '~/view/types';
import { View } from '../../';
import { IsStable$, ParentContainer, InputSource } from '../types';
import { isView } from '../../type-guards';


type ChildrenInitResult = [Container[], IsStable$[]];

export abstract class Container {

  protected subscriptions: Subscription = new Subscription();
  protected children: Container[] = [];

  protected constructor(
    protected parent: ParentContainer // TODO do I need it?
  ) {}

  abstract onInit(): IsStable$;

  onDestroy(): void {
    this.subscriptions.unsubscribe();
    this.destroyChildContainers();
    this.destroyParent();
  }

  getChildren(): Container[] {
    return this.children;
  }

  protected abstract createChildContainer(view: View): Container;

  protected onViewConnected(updatedView: PossibleView, children: Container[] = this.getChildren()): void {
    children.forEach(child => child.onViewConnected(updatedView));
  }

  protected initChildContainers(views: View | HTMLCollection, source?: InputSource): IsStable$ {
    const children: View[] = isView(views) ? [views] : (Array.from(views) as View[]);
    const [containers, stables]: ChildrenInitResult = children.reduce(

      (acc: ChildrenInitResult, view: View): ChildrenInitResult => {
        const container: Container = this.createChildContainer(view);
        return [[...acc[0], container], [...acc[1], container.onInit()]];
      },
      [[], []]
    );

    this.children = containers;

    return forkJoin(!!stables.length ? stables : [stable()]).pipe(mapTo(true));
  }

  protected destroyChildContainers(): void {
    this.children.forEach(child => child.onDestroy());
    this.children = [];
  }

  private destroyParent(): void {
    this.parent = null;
  }
}

export function stable(): IsStable$ {
  return of(true);
}
