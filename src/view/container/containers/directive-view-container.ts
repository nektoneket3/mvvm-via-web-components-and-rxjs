import { Injector, COMPONENT, VIEW } from '~/di';
import { Directive, View } from '../../types';
import { IsStable$, ParentContainer, DirectiveLocalDependencies } from '../types';
import { Container } from './container';
import { ContainerFactory } from '../container-factory';
import { BindingParser } from '../../directive/parsers/binding-parser';
import { BindingSource } from '../../directive/parsers/types';
import { DirectiveBuilder } from '../meta-class-builder/meta-class-builder';


export abstract class DirectiveViewContainer extends Container {

  protected directives: Directive[] = [];

  constructor(
    protected view: View,
    protected bindingSource: BindingSource,
    parent: ParentContainer,
    protected containerFactory: ContainerFactory,
    protected injector: Injector,
    protected bindingParser: BindingParser,
  ) {
    super(parent);
  }

  onInit(): IsStable$ {
    this.setDirectives(this.view);
    return this.onDirectivesSet(this.view);
  }

  onDestroy(): void {
    this.destroyDirectives();
    super.onDestroy();
  }

  protected abstract createDirectiveBuilders(localDependencies: DirectiveLocalDependencies): DirectiveBuilder[];

  protected abstract onDirectivesSet(view: View): IsStable$;

  protected setDirectives(view: View): void {
    const localDependencies = { [VIEW]: view, [COMPONENT]: this.bindingSource.component };
    this.directives = this.createDirectiveBuilders(localDependencies).map((directiveBuilder) => {
      const directive = directiveBuilder
        .create()
        .setMetaProps()
        .setBindableSubscriptions()
        .setInputSubscriptions(this.bindingSource)
        .setOutputSubscriptions(this.bindingSource)
        .getResult();
      this.subscriptions.add(directiveBuilder.getSubscriptions());
      directive.onInit?.();
      return directive;
    });
  }

  private destroyDirectives(): void {
    this.directives.forEach(directive => directive.onDestroy?.());
    this.directives = [];
  }
}
