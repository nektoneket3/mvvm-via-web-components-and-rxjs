import { Observable } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';

import { Injector } from '~/di';
import { PossibleView, View } from '../../types';
import { ParentContainer, StructuralDirectiveLocalDependencies } from '../types';
import { isComment } from '../../type-guards';
import { Container } from './container';
import { StructuralViewContainer } from './structural-view-container';
import { ContainerFactory } from '../container-factory';
import { BindingSource } from '../../directive/parsers/types';
import { DirectiveBuilder } from '../meta-class-builder/meta-class-builder';
import { IfDirectiveBindingParser } from '../../directive/parsers/if-directive-binding-parser';
import { IfDirective } from '../../directive/structural-directives/if.directive';


type InputSource = [boolean, boolean];

export class IfDirectiveViewContainer extends StructuralViewContainer {

  static create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): IfDirectiveViewContainer | null {
    const bindingParser: IfDirectiveBindingParser = new IfDirectiveBindingParser(view);

    return bindingParser.viewContainsDirective('if')
      ? new IfDirectiveViewContainer(view, bindingSource, parent, containerFactory, injector, bindingParser)
      : null;
  }

  protected createDirectiveBuilder(localDependencies: StructuralDirectiveLocalDependencies): DirectiveBuilder {
    return new DirectiveBuilder(IfDirective, this.injector, localDependencies, this.bindingParser);
  }

  protected createChildContainer(view: View): Container {
    return this.containerFactory
      .excludeIfContainer()
      .excludeRepeatContainer()
      .create(view, this.bindingSource, this);
  }

  protected getInputSource(): Observable<InputSource> {
    const source = this.bindingParser.getInputSource('if', this.bindingSource);
    return source.pipe(startWith(false), pairwise());
  }

  protected beforeChildContainersInit(): void {
    this.destroyChildContainers();
  }

  protected onUpdatedViewReceived(updatedView: PossibleView): void {
    if (!(isComment(updatedView) && isComment(this.currentView) && updatedView.data === this.currentView.data)) {
      this.replaceElement(updatedView);
    }
  }

  protected onViewConnected(updatedView: PossibleView) {
    if (this.currentView.isConnected) {
      super.onViewConnected(updatedView);
    }
  }

  protected replaceElement(possibleView: PossibleView): void {
    if (!this.elementContentReplacement) {
      this.elementContentReplacement = possibleView;
      this.elementContent!.replaceWith(possibleView);
      this.elementContent = null;
    } else {
      this.elementContent = possibleView;
      this.elementContentReplacement.replaceWith(possibleView);
      this.elementContentReplacement = null;
    }
  }
}
