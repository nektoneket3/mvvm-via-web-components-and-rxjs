import { forkJoin, of, Observable } from 'rxjs';
import { concatMap, map, mapTo, tap, startWith, pairwise } from 'rxjs/operators';

import { Injector } from '~/di';
import { PossibleView, View } from '../../types';
import { IsStable$, ParentContainer, StructuralDirectiveLocalDependencies } from '../types';
import { Container, stable } from './container';
import { StructuralViewContainer } from './structural-view-container';
import { ContainerFactory } from '../container-factory';
import { BindingSource, RepeatableIndex } from '../../directive/parsers/types';
import { DirectiveBuilder } from '../meta-class-builder/meta-class-builder';
import { RepeatDirectiveBindingParser } from '../../directive/parsers/repeat-directive-binding-parser';
import { RepeatDirective } from '../../directive/structural-directives/repeat.directive';


type MountedViewIndexes = number[];
type InputSource = [unknown[], unknown[]];

export class RepeatDirectiveViewContainer extends StructuralViewContainer {

  private commentedView!: PossibleView;
  private views: View[] = [];
  private mountedViewIndexes: MountedViewIndexes = [];

  static create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): RepeatDirectiveViewContainer | null {
    const bindingParser: RepeatDirectiveBindingParser = new RepeatDirectiveBindingParser(view);

    return bindingParser.viewContainsDirective('repeat')
      ? new RepeatDirectiveViewContainer(view, bindingSource, parent, containerFactory, injector, bindingParser)
      : null;
  }

  onDestroy(): void {
    this.views.forEach(view => view.remove()); // TODO do I need it?
    super.onDestroy();
  }

  protected createDirectiveBuilder(localDependencies: StructuralDirectiveLocalDependencies): DirectiveBuilder {
    return new DirectiveBuilder(RepeatDirective, this.injector, localDependencies, this.bindingParser);
  }

  protected createChildContainer(view: View, bindingSource?: BindingSource): Container {
    return this.containerFactory
      .excludeIfContainer()
      .excludeRepeatContainer()
      .create(view, bindingSource!, this);
  }

  protected getInputSource(): Observable<InputSource> {
    const source = this.bindingParser.getInputSource('repeat', this.bindingSource);
    return source.pipe(startWith([]), pairwise());
  }

  /**
   * It compares prev and current array of items, finds new elements and creates for them containers.
   * For elements which were removed it calls onDestroy and remove them.
   */
  protected initChildContainers(view: View, source: InputSource): IsStable$ {
    const sourceName = this.bindingParser.getViewBindingAttr('repeat')!.value.split(' in ')[0]; // TODO refactor it

    return of(source).pipe(
      map<InputSource, [Container[], IsStable$[]]>(([prevItems, newItems]) => {
        const children = this.getChildren();
        const views = (Array.from(view.children) as View[]);

        if (!newItems.length) {
          this.destroyChildContainers();
          return [[], [stable()]];
        }

        if (prevItems.length > newItems.length) {
          this.children.slice(newItems.length).forEach(child => child.onDestroy?.());
        }

        return newItems.reduce(
          (acc: [Container[], IsStable$[]], repeatData: unknown, index: number): [Container[], IsStable$[]] => {
            if (repeatData === prevItems[index]) {
              return [[...acc[0], children[index]], [...acc[1], stable()]];
            }
            children[index]?.onDestroy?.();
            const container: Container = this.createChildContainer(
              views[index],
              this.createBindingSource(sourceName, repeatData, index)
            );
            return [[...acc[0], container], [...acc[1], container.onInit()]];
          },
          [[], []]
        );
      }),
      tap(([containers]) => this.children = containers),
      map(([, stables]) => stables),
      concatMap<IsStable$[], IsStable$>((stables: IsStable$[]) => forkJoin(stables).pipe(mapTo(true)))
    );
  }

  protected onUpdatedViewReceived(updatedView: PossibleView, [prevItems, newItems]: InputSource): void {
    const views = (Array.from((updatedView as View).children) as View[]);

    if (!newItems.length) {
      this.views = this.views.filter(view => view.remove());
      this.mountedViewIndexes = [];
      return;
    }
    if (!prevItems.length) {
      this.views = views;
      this.mountedViewIndexes = views.map((view, index) => index);
      this.commentedView.after(...views);
      return;
    }
    if (prevItems.length > newItems.length) {
      this.views.slice(newItems.length).forEach(view => view.remove());
    }

    const result = newItems.reduce(
      ([resultViews, mountedViewIndexes]: [View[], MountedViewIndexes], newItem, index: number): [View[], MountedViewIndexes] => {
        const oldView: View | void = this.views[index];
        const newView: View = views[index];

        if (newItem === prevItems[index]) {
          return [[...resultViews, oldView], mountedViewIndexes];
        }

        oldView ? oldView.replaceWith(newView) : resultViews[index - 1].after(newView);

        return [[...resultViews, newView], [...mountedViewIndexes, index]];
      },
      [[], []]
    );

    this.views = result[0];
    this.mountedViewIndexes = result[1];
  }

  protected onViewConnected(updatedView: PossibleView): void {
    const mountedChildContainers = this.mountedViewIndexes.reduce(
      (acc: Container[], mountedViewIndex: number): Container[] => this.views[mountedViewIndex].isConnected
        ? [...acc, this.getChildren()[mountedViewIndex]]
        : acc,
      []
    );
    if (mountedChildContainers.length) {
      super.onViewConnected(updatedView, mountedChildContainers);
      this.directives.forEach(directive => directive.onViewConnected?.next?.());
    }
  }

  protected replaceElement(commentedView: PossibleView): void {
    this.commentedView = commentedView;
    this.elementContent!.replaceWith(commentedView);
  }

  private createBindingSource(sourceName: string, repeatData: unknown, index: number): BindingSource {
    const repeatableIndex: RepeatableIndex = '$$index';
    return {
      ...this.bindingSource,
      scope: {
        ...(this.bindingSource.scope ? { parent: this.bindingSource.scope } : {}),
        scope: { [sourceName]: repeatData, [repeatableIndex]: index }
      }
    };
  }
}
