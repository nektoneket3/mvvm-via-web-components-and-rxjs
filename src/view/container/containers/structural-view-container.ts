import { Observable, ReplaySubject, zip } from 'rxjs';
import { concatMap, mapTo, tap } from 'rxjs/operators';

import { VIEW_UPDATED } from '~/di';
import { PossibleView, UpdatedView$, View } from '../../types';
import { StructuralDirectiveLocalDependencies, IsStable$, DirectiveLocalDependencies, InputSource } from '../types';
import { DirectiveViewContainer } from './directive-view-container';
import { Container, stable } from './container';
import { isView } from '../../type-guards';
import { DirectiveBuilder } from '../meta-class-builder/meta-class-builder';


export abstract class StructuralViewContainer extends DirectiveViewContainer {

  protected elementContent: PossibleView | null = null;
  protected elementContentReplacement: PossibleView | null = null;
  protected updatedView: UpdatedView$ = new ReplaySubject(1);

  get currentView(): PossibleView {
    return (this.elementContent || this.elementContentReplacement)!;
  }

  onInit(): IsStable$ {
    this.isStructuralDirectiveSingle();
    return super.onInit();
  }

  protected abstract createDirectiveBuilder(localDependencies: StructuralDirectiveLocalDependencies): DirectiveBuilder;

  protected abstract replaceElement(possibleView: PossibleView): void;

  protected abstract onUpdatedViewReceived(updatedView: PossibleView, source?: InputSource): void;

  protected abstract getInputSource(): Observable<InputSource>;

  protected createDirectiveBuilders(localDependencies: DirectiveLocalDependencies): DirectiveBuilder[] {
    return [this.createDirectiveBuilder({ ...localDependencies, [VIEW_UPDATED]: this.updatedView })];
  }

  protected onDirectivesSet(view: View): IsStable$ {
    this.elementContent = this.view;
    this.replaceElement(document.createComment(view.outerHTML));
    this.initViewUpdate();
    return stable();
  }

  protected beforeChildContainersInit(): void {
    // pass
  }

  protected onViewConnected(updatedView: PossibleView, children?: Container[]): void {
    if (isView(updatedView)) {
      super.onViewConnected(updatedView, children);
    }
  }

  private initViewUpdate(): void {
    this.subscriptions.add(
      zip(this.updatedView, this.getInputSource()).pipe(
        tap(() => this.beforeChildContainersInit()),
        concatMap<[PossibleView, InputSource], Observable<[PossibleView, InputSource]>>(([view, source]) => {
          const isStable = isView(view) ? this.initChildContainers(view, source) : stable();
          return isStable.pipe(mapTo([view, source]));
        })
      ).subscribe(([updatedView, source]) => {
        this.onUpdatedViewReceived(updatedView, source);
        this.onViewConnected(updatedView);
      })
    );
  }

  private isStructuralDirectiveSingle(): true | never {
    if (['if', 'repeat'].every(directiveName => this.bindingParser.viewContainsDirective(directiveName))) {
      throw new Error(`Applied more then 1 structural directive on ${this.view.localName} element.`);
    }
    return true;
  }
}
