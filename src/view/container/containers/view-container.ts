import { Injector } from '~/di';
import { View } from '../../types';
import { IsStable$, ParentContainer } from '../types';
import { Container } from './container';
import { ContainerFactory } from '../container-factory';
import { BindingSource } from '../../directive/parsers/types';


/**
 * ViewContainer is responsible for DOM node (view) updates based on data source (component) and creating
 * child ViewContainers or ComponentContainers if need.
 * Each ViewContainer.children can contain ViewContainers and ComponentContainers.
 */
export class ViewContainer extends Container {

  constructor(
    protected view: View,
    protected bindingSource: BindingSource,
    parent: ParentContainer,
    protected containerFactory: ContainerFactory,
    private injector: Injector
  ) {
    super(parent);
  }

  static create(view: View, bindingSource: BindingSource, parent: ParentContainer, containerFactory: ContainerFactory, injector: Injector): ViewContainer {
    return new ViewContainer(view, bindingSource, parent, containerFactory, injector);
  }

  onInit(): IsStable$ {
    return this.initChildContainers(this.view.children);
  }

  protected createChildContainer(view: View): Container {
    return this.containerFactory.allowAll().create(view, this.bindingSource, this);
  }
}
