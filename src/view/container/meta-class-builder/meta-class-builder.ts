import { Subscription } from 'rxjs';

import { DependencyInstance, Injector, LocalInjector, SUBSCRIPTIONS } from '~/di';
import { Component } from '~/component';
import { classMetadata, ClassMetadata, DependencyName, Metadata } from '~/metadata';
import { BindableSubscriptions } from '~/decorator';
import { ClassCtor, ClassInstance } from '~/types';
import { ComponentLocalDependencies, DirectiveLocalDependencies, LocalDependencies } from '../types';
import { BindingParser } from '../../directive/parsers/binding-parser';
import { BindingSource } from '../../directive/parsers/types';
import { SourceBinder } from './source-binder';
import { MetaObjectFactory } from './meta-object-factory';
import { Directive } from '../../types';


abstract class MetaClassBuilder<T extends ClassInstance> {

  private metadata: Metadata<ClassMetadata>;
  private classInstance!: T;
  private sourceBinder: SourceBinder;
  private subscriptions: Subscription = new Subscription();
  private bindableSubscriptions: BindableSubscriptions = {};

  protected constructor(
    private ctor: ClassCtor<T>,
    private injector: Injector,
    private localDependencies: LocalDependencies,
    bindingParser: BindingParser
  ) {
    this.metadata = classMetadata(ctor);
    this.sourceBinder = new SourceBinder(this.metadata.get(), bindingParser);
  }

  create(): this {
    this.classInstance = new this.ctor(...this.getDependencies());
    return this;
  }

  setMetaProps(): this {
    const metaObjectFactory = new MetaObjectFactory(this.metadata.get());
    Object
      .entries(metaObjectFactory.create(this.classInstance))
      .forEach(([propName, propValue]) => this.setProp(propName, propValue));
    Object
      .entries(metaObjectFactory.getWithLatestProps(this.classInstance))
      .forEach(([propName, propValue]) => this.setProp(propName, propValue));
    return this;
  }

  setBindableSubscriptions(): this {
    Object.entries(this.sourceBinder.setBindableSubscriptions(this.classInstance)).forEach(
      ([subscriptionName, subscription]) => this.bindableSubscriptions[subscriptionName] = subscription
    );
    return this;
  }

  setInputSubscriptions(bindingSource: BindingSource): this {
    this.subscriptions.add(this.sourceBinder.bindInputs(bindingSource, this.classInstance));
    return this;
  }

  setOutputSubscriptions(bindingSource: BindingSource): this {
    this.subscriptions.add(this.sourceBinder.bindOutputs(bindingSource, this.classInstance));
    return this;
  }

  getResult(): T {
    return this.classInstance;
  }

  getSubscriptions(): Subscription {
    return Object.values(this.bindableSubscriptions).reduce(
      (subscriptions: Subscription, subscription: Subscription): Subscription => {
        subscriptions.add(subscription);
        return subscriptions
      },
      this.subscriptions
    );
  }

  private getDependencies(): DependencyInstance[] { // TODO must be refactored
    const initialDependencyNames = [...(this.metadata.get().dependencies || [])];
    const localInjector = new LocalInjector({}, { ...this.localDependencies, [SUBSCRIPTIONS]: this.bindableSubscriptions });
    const dependencies = localInjector.getDependencies(this.ctor);

    const [localDependencies, restDependencyNames] = dependencies.reduce(
      (acc: [Record<string, DependencyInstance>, DependencyName[]], dependency, index: number): [Record<string, DependencyInstance>, DependencyName[]] => {
        if (typeof dependency === 'string') {
          return [acc[0], [...acc[1], dependency]];
        }
        return [{ ...acc[0], [index]: dependency }, acc[1]];
      },
      [{}, []]
    );

    this.metadata.update({ dependencies: restDependencyNames });
    const restDependencies = this.injector.getDependencies(this.ctor);
    this.metadata.update({ dependencies: initialDependencyNames });

    Object.entries(localDependencies).forEach(([index, dependency]) => {
      restDependencies.splice(+index, 0, dependency);
    });

    return restDependencies;
  }

  private setProp(propName: string, value: any): void {
    (this.classInstance as ClassInstance)[propName] = value;
  }
}

export class ComponentBuilder extends MetaClassBuilder<Component> {

  constructor(
    ctor: ClassCtor<Component>,
    injector: Injector,
    localDependencies: ComponentLocalDependencies,
    bindingParser: BindingParser
  ) {
    super(ctor, injector, localDependencies, bindingParser);
  }
}

export class DirectiveBuilder extends MetaClassBuilder<Directive> {

  constructor(
    ctor: ClassCtor<Directive>,
    injector: Injector,
    localDependencies: DirectiveLocalDependencies,
    bindingParser: BindingParser
  ) {
    super(ctor, injector, localDependencies, bindingParser);
  }
}
