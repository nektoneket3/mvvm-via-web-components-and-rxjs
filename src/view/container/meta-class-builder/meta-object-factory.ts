import { BehaviorSubject, ReplaySubject, Subject, Subscription } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';

import { BindableMetadata, ClassMetadata, PropertyKey } from '~/metadata';
import { ClassInstance } from '~/types';
import { sourceIsCallback, sourceIsValue } from '~/decorator/type-guards';


type BindableProp = [PropertyKey, BindableMetadata];

export class MetaObjectFactory {

  constructor(
    private metadata: ClassMetadata
  ) {}
  /**
   * Gets decorated properties from component/directive class (using its metadata) and creates a plain object
   * with these properties but with appropriate values.
   * Example:
   *   class ComponentName {
   *     @input() inputName: Input<any>;
   *     @output() outputName: Output<any>;
   *     @bindable('test data') bindablePropName: Bindable<string>;
   *   }
   *
   * create() will return next object:
   *   {
   *     inputName: new ReplaySubject(1),
   *     outputName: new Subject(),
   *     bindablePropName: new BehaviorSubject('test data')
   *   }
   */
  create(originalClassInstance: ClassInstance): ClassInstance {
    return this.validateAndMerge(
      this.getInputs(),
      this.getOutputs(),
      this.getBindables(originalClassInstance),
    );
  }

  getWithLatestProps(originalClassInstance: ClassInstance): ClassInstance {
    return Object.entries(this.metadata.bindableProperties || {}).reduce(
      (acc: ClassInstance, [propertyKey, { source, withLatest }]: BindableProp): ClassInstance => {
        if (withLatest) {
          acc[propertyKey] = new Subject().pipe(withLatestFrom(withLatest(originalClassInstance)));
        }
        return acc;
      },
      {}
    )
  }

  private getInputs(): ClassInstance {
    return Object.values(this.metadata.inputs || {}).reduce(
      (acc: ClassInstance, { propertyName, defaultValue }): ClassInstance => {
        return {
          ...acc,
          [propertyName]: typeof defaultValue !== 'undefined'
            ? new BehaviorSubject<any>(defaultValue)
            : new ReplaySubject<any>(1)
        };
      },
      {}
    );
  }

  private getOutputs(): ClassInstance {
    return Object.values(this.metadata.outputs || {}).reduce(
      (acc: ClassInstance, { propertyName }): ClassInstance => {
        return { ...acc, [propertyName]: new Subject() };
      },
      {}
    );
  }

  private getBindables(classInstance: ClassInstance): ClassInstance {
    return Object.entries(this.metadata.bindableProperties || {}).reduce(
      (acc: ClassInstance, [propertyKey, { source, bindable }]: BindableProp): ClassInstance => {
        if (!bindable) {
          throw new Error(`class property ${propertyKey} was not decorated with @bindable() or @subscribedBindable().`);
        }

        if (!source) {
          acc[propertyKey] = new Subject();

        } else if (sourceIsValue(source)) {
          acc[propertyKey] = new BehaviorSubject<any>(source.fromValue);

        } else if (sourceIsCallback(source)) {
          acc[propertyKey] = this.subscribeInterceptor(
            new ReplaySubject(1),
            (target: ReplaySubject<any>): Subscription | void => {
              if (target.observers.length === 0) {
                return source.fromCallback(classInstance).subscribe((value: any) => target.next(value))
              }
            }
          );
        }
        return acc;
      },
      {}
    );
  }

  private validateAndMerge(...sources: ClassInstance[]): ClassInstance | never {
    return sources.reduce(
      (acc: ClassInstance, source: ClassInstance): ClassInstance => {
        return Object.entries(source).reduce(
          (result: ClassInstance, [propName, PropValue]: [string, any]): ClassInstance => {
            if (result.hasOwnProperty(propName)) {
              throw new Error(`Property ${propName} can be decorated only once with @input, @output or @bindable decorator.`)
            }
            return { ...result, [propName]: PropValue };
          },
          acc
        );
      },
      {}
    );
  }
  /**
   * Returned interceptor object is responsible for subscribing on each bindable's source from callback when
   * bindable prop get its first subscription.
   * Example: @bindable(() => of('value'))
   * When bindable get subscribers - bindable sourceFromCallBack (in our case () => of('value')) will be called
   * and result (Observable) get its subscription and will start emitting values.
   */
  private subscribeInterceptor<T extends { [key: string]: any, subscribe: Function }>(target: T, beforeCallCb: Function): T {
    const wrappedSubscribe = (thisArg: any, originalMethod: Function, argumentsList: any) => {
      const beforeCbSubscription: Subscription = beforeCallCb(thisArg);
      const originalSubscription: Subscription = originalMethod.apply(thisArg, argumentsList);
      originalSubscription.add(beforeCbSubscription);
      return originalSubscription;
    };

    if (Proxy) {
      target.subscribe = new Proxy(target.subscribe, {
        apply(targetFunc: Function, thisArg: T, argumentsList: any) {
          return wrappedSubscribe(thisArg, targetFunc, argumentsList);
        }
      });
    } else { // same logic but without Proxy if it is not Supported
      const originalMethod: Function = target.subscribe;
      target.subscribe = function(this: T): Subscription {
        return wrappedSubscribe(this, originalMethod, arguments);
      };
    }

    return target;
  }
}
