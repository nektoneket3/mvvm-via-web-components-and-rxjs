import { Observable, Subject, Subscription } from 'rxjs';

import { BindableMetadata, ClassMetadata, PropertyKey } from '~/metadata';
import { BindableSubscriptions } from '~/decorator';
import { Input, Output } from '~/component';
import { ClassInstance } from '~/types';
import { isBindableOrCallback, isOutput, isInput } from '~/type-guards';
import { BindingParser } from '../../directive/parsers/binding-parser';
import { BindingName, BindingSource } from '../../directive/parsers/types';


type OuterInputSource = Observable<any> | never; // TODO move to types
type OuterOutputSource = Subject<any> | Function | never; // TODO move to types
type BindableProp = [PropertyKey, BindableMetadata];

export class SourceBinder {

  constructor(
    private metadata: ClassMetadata,
    private bindingParser: BindingParser
  ) {}
  /**
   * If @bindable() get subscribe callback or used @subscribe() decorator
   * then subscribe to bindable property and call that callback.
   */
  setBindableSubscriptions(bindablesSource: ClassInstance): BindableSubscriptions {
    return Object.entries(this.metadata.bindableProperties || {}).reduce(
      (acc: BindableSubscriptions, [propertyKey, { subscribe }]: BindableProp): BindableSubscriptions => {
        if (subscribe) {
          acc[propertyKey] = bindablesSource[propertyKey].subscribe((value: unknown) => subscribe(value, bindablesSource));
        }
        return acc;
      },
      {}
    );
  }
  /**
   * Find all element attributes with bindings (data-bind-binding-name) and connect bindables from outer
   * data source (component/directive scope) with inputs in inner data source (component/directive).
   */
  bindInputs(bindingSource: BindingSource, inputsSource: ClassInstance): Subscription {
    return Object.entries(this.metadata.inputs || {}).reduce(
      (subscriptions: Subscription, [key, { publicPropertyName, propertyName }]): Subscription => {

        if (this.bindingParser.viewContainsBinding(publicPropertyName)) {
          const outerSource$ = this.getOuterInputSource(publicPropertyName, bindingSource);
          const input$: Input<unknown> = inputsSource[propertyName];

          if (!(isInput(input$))) { // TODO refactor it
            throw new Error(`Type error: ${propertyName} must be a BehaviorSubject or ReplaySubject or decorated with @input().`);
          }

          subscriptions.add(outerSource$.subscribe(input$)); // TODO check if input$ can be passed like that (seems like it works, but need double check)
        }
        return subscriptions;
      },
      new Subscription()
    );
  }
  /**
   * Find all element attributes with bindings (data-bind-binding-name) and connect outputs from inner
   * data source (component/directive) with bindables from outer data source (component).
   */
  bindOutputs(bindingSource: BindingSource, outputsSource: ClassInstance): Subscription {
    return Object.entries(this.metadata.outputs || {}).reduce(
      (subscriptions: Subscription, [key, { publicPropertyName, propertyName }]): Subscription => {

        if (this.bindingParser.viewContainsBinding(publicPropertyName)) {
          const outputAttr = this.bindingParser.getViewBindingAttr(publicPropertyName)!;
          const outerSource$ = this.getOuterOutputSource(publicPropertyName, bindingSource);
          const output$: Output<unknown> = outputsSource[propertyName];

          if (!isOutput(output$)) { // TODO refactor it
            throw new Error(`Type error: ${propertyName} must be a Subject or decorated with @output().`);
          }

          subscriptions.add(
            output$.subscribe((value: unknown) => {
              isBindableOrCallback(outerSource$) ? outerSource$.next(value) : bindingSource.component[outputAttr.value](value);
            })
          );
        }
        return subscriptions;
      },
      new Subscription()
    );
  }

  private getOuterInputSource(bindingName: BindingName, bindingSource: BindingSource): OuterInputSource {
    return this.bindingParser.getInputSource(bindingName, bindingSource);
  }

  private getOuterOutputSource(bindingName: BindingName, bindingSource: BindingSource): OuterOutputSource {
    return this.bindingParser.getOutputSource(bindingName, bindingSource);
  }
}
