import { BehaviorSubject, of, Subscription } from 'rxjs';

import { Component } from '../../../../component';
import { ContainerFactory } from '../../container-factory';
import { View } from '../../../';
import { ViewContainer } from '../view-container';
import { getElementFromString} from '../../../../helpers';


type Cases = { component: Component, expectedViewString: string, expectedChildren: any }[];

export function bindIfTest(containerFactory: ContainerFactory): void {

  const viewString: string = `<div><div data-bind-if="condition1$"><div data-bind-if="condition2$"><div data-bind-if="condition3$">test</div></div></div></div>`;

  describe(`\n    bind-if-test.ts: template: ${viewString}`, () => {

    let isStableSubscription: Subscription;
    let view: View;

    beforeEach(() => {
      view = getElementFromString(viewString);
    });

    afterEach(() => {
      isStableSubscription.unsubscribe();
    });

    const cases: Cases = [
      {
        component: {
          condition1$: of(true),
          condition2$: of(true),
          condition3$: of(true),
        },
        expectedViewString: viewString,
        expectedChildren: [{ children: [{ children: [{ children: [{ children: [{ children: [{ children: [] }] }] }] }] }] }]
      },
      {
        component: {
          condition1$: of(false),
          condition2$: of(true),
          condition3$: of(true),
        },
        expectedViewString: '<div><!--<div data-bind-if="condition1$"><div data-bind-if="condition2$"><div data-bind-if="condition3$">test</div></div></div>--></div>',
        expectedChildren: [{ children: [] }]
      },
      {
        component: {
          condition1$: of(true),
          condition2$: of(false),
          condition3$: of(true),
        },
        expectedViewString: '<div><div data-bind-if="condition1$"><!--<div data-bind-if="condition2$"><div data-bind-if="condition3$">test</div></div>--></div></div>',
        expectedChildren: [{ children: [{ children: [{ children: [] }] }] }]
      },
      {
        component: {
          condition1$: of(true),
          condition2$: of(true),
          condition3$: of(false),
        },
        expectedViewString: '<div><div data-bind-if="condition1$"><div data-bind-if="condition2$"><!--<div data-bind-if="condition3$">test</div>--></div></div></div>',
        expectedChildren: [{ children: [{ children: [{ children: [{ children: [{ children: [] }] }] }] }] }]
      }
    ];

    cases.forEach(({ component, expectedViewString, expectedChildren }) => {

      test(`getChildren()`, (done) => { // TODO implement it

        const viewContainer: ViewContainer = containerFactory.createViewContainer(view, component, null);

        isStableSubscription = viewContainer.onInit().subscribe(() => {
          expect(viewContainer.getChildren()).toMatchObject(expectedChildren);
          done();
        });

      });

      test(`expectedViewString: ${expectedViewString}`, (done) => {

        const viewContainer: ViewContainer = containerFactory.createViewContainer(view, component, null);

        isStableSubscription = viewContainer.onInit().subscribe(() => {
          expect((viewContainer.currentView as View).outerHTML.trim()).toEqual(expectedViewString);
          done();
        });

      });

    });
  });
}

export function bindIfWithChangedStateTest(containerFactory: ContainerFactory): void {

  const viewString: string = `<div><div data-bind-if="condition1$"></div></div>`;

  describe(`\n    bind-if-test.ts: changedState: template: ${viewString}`, () => {

    let isStableSubscription: Subscription;
    let view: View;

    beforeEach(() => {
      view = getElementFromString(viewString);
    });

    afterEach(() => {
      isStableSubscription.unsubscribe();
    });

    const component = {
      condition1$: new BehaviorSubject(true),
    };

    const expectedViewString = '<div><!--<div data-bind-if="condition1$"></div>--></div>';

    test(`condition1$ === true, condition1$ === false, condition1$ === true`, (done) => {

      const viewContainer: ViewContainer = containerFactory.createViewContainer(view, component, null);

      isStableSubscription = viewContainer.onInit().subscribe(() => {

        expect((viewContainer.currentView as View).outerHTML.trim()).toEqual(viewString);

        component.condition1$.next(false);

        expect((viewContainer.currentView as View).outerHTML.trim()).toEqual(expectedViewString);

        component.condition1$.next(true);

        expect((viewContainer.currentView as View).outerHTML.trim()).toEqual(viewString);

        done();
      });

    });
  });
}
