import { Subscription } from 'rxjs';

import { ViewContainer } from '../view-container';
import { ContainerFactory } from '../../container-factory';
import { View } from '../../../';
import { getElementFromString } from '../../../../helpers';
import { NoChildren, OneChild, AllChildrenWithChildren, TwoChildren, FirstChildWithChildren, FirstChildIsComponent } from './types';


type PossibleExpectedChildren = NoChildren | OneChild | AllChildrenWithChildren | TwoChildren | FirstChildWithChildren | FirstChildIsComponent;
type Cases = { viewString: string, expectedChildren: PossibleExpectedChildren }[];

export function getChildrenTest(containerFactory: ContainerFactory): void {

  describe('\n    get-children-test.ts:', () => {

    let isStableSubscription: Subscription;

    afterEach(() => {
      isStableSubscription.unsubscribe();
    });

    const cases: Cases = [
      {
        viewString: `<div></div>`,
        expectedChildren: []
      },
      {
        viewString: `<div><div id="child-1"></div></div>`,
        expectedChildren: [{ children: [] }]
      },
      {
        viewString: `<div><div id="child-1"></div><div id="child-2"></div></div>`,
        expectedChildren: [{ children: [] }, { children: [] }]
      },
      {
        viewString: `<div><div id="child-1"><div id="child-1-child-1"></div></div><div id="child-2"></div></div>`,
        expectedChildren: [{ children: [{ children: [] }] }, { children: [] }]
      },
      {
        viewString: `<div><div id="child-1"><div id="child-1-child-1"></div></div><div id="child-2"><div id="child-2-child-1"></div></div></div>`,
        expectedChildren: [{ children: [{ children: [] }] }, { children: [{ children: [] }] }]
      },
      {
        viewString: `<div><div id="child-1"><test-component id="child-1-child-1"></test-component></div></div>`,
        expectedChildren: [{ children: [{ children: [{ children: [{ children: [] }] }] }] }]
      },
    ];

    cases.forEach(({ viewString, expectedChildren }) => {

      test(`children == ${JSON.stringify(expectedChildren)}`, (done) => {
        const view: View = getElementFromString(viewString);
        const viewContainer: ViewContainer = containerFactory.createViewContainer(view, {}, null);

        isStableSubscription = viewContainer.onInit().subscribe(() => {
          expect(viewContainer.getChildren()).toMatchObject(expectedChildren);
          done();
        });
      });
    });
  });
}
