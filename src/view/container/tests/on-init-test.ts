import { Observable, Subscription } from 'rxjs';

import { ContainerFactory } from '../../container-factory';
import { ViewContainer } from '../view-container';
import { getElementFromString } from '../../../../helpers';
import { IsStable } from '../../types';


export function onInitTest(containerFactory: ContainerFactory): void {

  describe('\n    on-init-test.ts:', () => {

    let isStableSubscription: Subscription;

    afterEach(() => {
      isStableSubscription.unsubscribe();
    });

    test('returns Observable<true>', (done) => {

      const viewContainer: ViewContainer = containerFactory.createViewContainer(
        getElementFromString('<div></div>'),
        {},
        null
      );

      const onInitResult: Observable<IsStable> = viewContainer.onInit();

      expect(onInitResult).toBeInstanceOf(Observable);

      isStableSubscription = onInitResult.subscribe((value) => {
        expect(value).toBe(true);
        done();
      });
    });
  });
}
