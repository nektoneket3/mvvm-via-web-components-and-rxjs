import { ContainerFactory } from '../../container-factory';
import { Injector } from '../../../../di';
import { onInitTest } from './on-init-test';
import { getChildrenTest } from './get-children-test';
import { bindIfTest, bindIfWithChangedStateTest } from './bind-if-test';
import { component } from '../../../../';


type Test = (containerFactory: ContainerFactory) => void

@component({
  name: 'test-component',
  template: '<div></div>',
})
export class TestComponentComponent {}

describe('view-container.ts', () => {

  const tests: Test[] = [onInitTest, getChildrenTest, bindIfTest, bindIfWithChangedStateTest];

  tests.forEach((test: Test) => {

    const injector = new Injector({ 'test-component': TestComponentComponent }, {});
    const containerFactory = new ContainerFactory(injector);

    test(containerFactory);

  });
});
