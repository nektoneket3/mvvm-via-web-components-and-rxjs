interface Child {
  children: []
}
interface ChildWithChildren {
  children: [Child]
}
export type NoChildren = [];
export type OneChild = [Child];
export type TwoChildren = [Child, Child];
export type FirstChildWithChildren = [ChildWithChildren, Child];
export type AllChildrenWithChildren = [ChildWithChildren, ChildWithChildren];
export type FirstChildIsComponent = [{ children: [{ children: [ChildWithChildren] }] }];
