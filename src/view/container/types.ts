import { Observable } from 'rxjs';

import { Component } from '~/component';
import { Container } from './containers/container';
import { UpdatedView$, View } from '../types';


export type IsStable = true;
export type IsStable$ = Observable<IsStable>;
export type ParentContainer = Container | null;

export interface LocalDependencies {
  view: View,
  [key: string]: any
}
export interface ComponentLocalDependencies extends LocalDependencies {}
export interface DirectiveLocalDependencies extends LocalDependencies {
  component: Component; // TODO do I need it ?
}
export interface StructuralDirectiveLocalDependencies extends DirectiveLocalDependencies {
  viewUpdated: UpdatedView$
}

type PrevValue = unknown;
type NewValue = unknown;
export type InputSource = [PrevValue, NewValue];
