import { DirectiveCtors, DirectiveCtorFactories } from '../types';
import { ClickDirective, MouseenterDirective, MouseleaveDirective } from './directives/event.directive';
import { ClassDirective } from './directives/class.directive';
import { ClassNameDirective } from './directives/class-name.directive';
import { StyleDirective } from './directives/style.directive';
import { inputDirectiveCtorFactory } from './directives/input/input-directive-ctor-factory';
import { ValueDirective } from './directives/value.directive';
import { AttrDirective } from './directives/attr.directive';
import { IfDirective } from './structural-directives/if.directive';
import { RepeatDirective } from './structural-directives/repeat.directive';


// TODO create names for each directive

export const attrBuiltInDirectives: DirectiveCtorFactories = {
  attr: AttrDirective,
  click: ClickDirective,
  mouseenter: MouseenterDirective,
  mouseleave: MouseleaveDirective,
  class: ClassDirective,
  'class-name': ClassNameDirective,
  style: StyleDirective,
  input: { factory: inputDirectiveCtorFactory },
  value: ValueDirective,
};

export const structuralBuiltInDirectives: DirectiveCtors = {
  if: IfDirective,
  repeat: RepeatDirective
};
