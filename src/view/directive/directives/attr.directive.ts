import { Observable, Subscription } from 'rxjs';

import { inject, VIEW } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { OnDestroy } from '~/types';
import { CurrentSubscriptions, View } from '../../types';


type AttrInput = Record<string, Observable<any>>;

const { bindable } = getBindableDecorators<AttrDirective>();

@inject(VIEW)
export class AttrDirective implements OnDestroy {

  @input() private attr!: Input<AttrInput>;

  @bindable(
    self => self.attr,
    (attrs, self) => {
      self.subscriptions.add(
        Object.entries(attrs).reduce(
          (subscriptions: CurrentSubscriptions, [attrName, bindingValue]: [string, Observable<any>]): CurrentSubscriptions => {
            subscriptions.add(
              bindingValue.subscribe((value: string) => self.view.setAttribute(attrName, value))
            );
            return subscriptions;
          },
          new Subscription()
        )
      );
    }
  )
  private attrChanged$!: Bindable<AttrInput>;

  private subscriptions: CurrentSubscriptions = new Subscription();

  constructor(
    private view: View,
  ) {}

  onDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
