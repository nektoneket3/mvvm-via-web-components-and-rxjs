import { filter, scan, distinctUntilChanged } from 'rxjs/operators';

import { inject, VIEW } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { View } from '../../types';


type ClassName = string;

const { bindable } = getBindableDecorators<ClassNameDirective>();

@inject(VIEW)
export class ClassNameDirective {

  @input({ name: 'class-name' }) private className!: Input<ClassName>;

  @bindable<ClassName>(
    (self) => self.className.pipe(
      scan((prevClassName, newClassName) => {
        if (prevClassName !== newClassName) {
          !!prevClassName && self.view.classList.remove(prevClassName);
          return newClassName;
        }
        return prevClassName;
      }),
      distinctUntilChanged(),
      filter(className => !!className)
    ),
    (className, self) => self.view.classList.add(className)
  )
  private classNameChanged!: Bindable<ClassName>;

  constructor(
    private view: View,
  ) {}
}
