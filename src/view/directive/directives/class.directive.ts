import { Observable, Subscription } from 'rxjs';

import { inject, VIEW } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { OnDestroy } from '~/types';
import { CurrentSubscriptions, View } from '../../types';


type Condition = boolean;
type Condition$ = Observable<Condition>;
type ClassName = string;
type ClassNameInput = Record<ClassName, Condition$>;

const { bindable } = getBindableDecorators<ClassDirective>();

@inject(VIEW)
export class ClassDirective implements OnDestroy {

  @input({ name: 'class' }) private className!: Input<ClassNameInput>;

  @bindable(
    self => self.className,
    (classNames, self) => {
      self.subscriptions.add(
        Object.entries(classNames).reduce(
          (subscriptions: CurrentSubscriptions, [className, binding]: [ClassName, Condition$]): CurrentSubscriptions => {
            subscriptions.add(
              binding.subscribe((condition: Condition) => {
                condition
                  ? self.view.classList.add(className)
                  : self.view.classList.remove(className);
              })
            );

            return subscriptions;
          },
          new Subscription()
        )
      );
    }
  )
  private classNameChanged!: Bindable<ClassNameInput>;

  private subscriptions: CurrentSubscriptions = new Subscription();

  constructor(
    private view: View,
  ) {}

  onDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
