import { fromEvent } from 'rxjs';

import { Output } from '~/component';
import { Bindable, getBindableDecorators, output } from '~/decorator';
import { inject, VIEW } from '~/di';
import { View } from '../../';


type EventName = 'click' | 'mouseenter' | 'mouseleave';

const { bindable } = getBindableDecorators<EventDirective>();

abstract class EventDirective {

  protected abstract event: Output<Event>;
  protected abstract eventName: EventName;

  @bindable<Event>(
    self => fromEvent(self.view, self.eventName),
    (event, self) => self.event.next(event)
  )
  protected eventEmitted$!: Bindable<Event>;

  constructor(
    private view: View,
  ) {}
}

@inject(VIEW)
export class ClickDirective extends EventDirective {
  @output('click') protected event!: Output<Event>;
  protected eventName: EventName = 'click';
}
@inject(VIEW)
export class MouseenterDirective extends EventDirective {
  @output('mouseenter') protected event!: Output<Event>;
  protected eventName: EventName = 'mouseenter';
}
@inject(VIEW)
export class MouseleaveDirective extends EventDirective {
  @output('mouseleave') protected event!: Output<Event>;
  protected eventName: EventName = 'mouseleave';
}
