type InputView = HTMLInputElement;

export abstract class InputHandler<T> {

  abstract inputChangedFilter(value: T): boolean;

  abstract setInput(value: T): void;

  abstract getInput(): T;

  constructor(
    protected inputView: InputView,
  ) {}
}

type TextValue = string;

export class TextInputHandler extends InputHandler<TextValue> {

  inputChangedFilter(value: TextValue): boolean {
    return this.inputView.value !== value
  }

  setInput(value: TextValue): void {
    this.inputView.value = value
  }

  getInput(): TextValue {
    return this.inputView.value;
  }
}

type CheckboxValue = boolean;

export class CheckboxInputHandler extends InputHandler<CheckboxValue> {

  inputChangedFilter(value: CheckboxValue): boolean {
    return this.inputView.checked !== value
  }

  setInput(value: CheckboxValue): void {
    this.inputView.checked = value
  }

  getInput(): CheckboxValue {
    return this.inputView.checked;
  }
}
