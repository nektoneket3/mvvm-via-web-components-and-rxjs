import { ClassCtor } from '~/types';
import { DirectiveCtorFactory, View } from '../../../types';
import { InputDirective } from './input.directive';


export const inputDirectiveCtorFactory: DirectiveCtorFactory = (view: View): ClassCtor<InputDirective<any>> => {
  if (!(view instanceof HTMLInputElement)) {
    throw new Error(`data-bind-input can be only applied on ${HTMLInputElement.name}, but not ${HTMLElement.name}`);
  }
  return InputDirective;
};
