import { fromEvent } from 'rxjs';
import { map, filter, withLatestFrom, distinctUntilChanged } from 'rxjs/operators';

import { inject, VIEW } from '~/di';
import { Input, Output } from '~/component';
import { Bindable, getBindableDecorators, input, output } from '~/decorator';
import { InputHandler, TextInputHandler, CheckboxInputHandler } from './handlers';


type InputView = HTMLInputElement;
type InputValue = string | boolean;
type InputTypeValue = 'text' | 'checkbox';

const { bindable } = getBindableDecorators<InputDirective>();

@inject(VIEW)
export class InputDirective<T = InputValue> {

  @input('text') type!: Input<InputTypeValue>;
  @input() input!: Input<T>;

  @output() change!: Output<T>;

  @bindable<InputTypeValue>(self => self.type.pipe(distinctUntilChanged()))
  private typeChanged!: Bindable<InputTypeValue>;

  @bindable<InputTypeValue>(
    self => self.typeChanged,
    (typeValue, self) => self.inputView.setAttribute('type', typeValue)
  )
  private setAttrType!: Bindable<InputTypeValue>;

  @bindable<InputHandler<InputValue>>(self => self.typeChanged.pipe(
    map((type) => {
      switch (type) {
        case 'checkbox':
          return new CheckboxInputHandler(self.inputView);
        case 'text':
        default:
          return new TextInputHandler(self.inputView);
      }
    })
  ))
  private inputHandler!: Bindable<InputHandler<InputValue>>;

  @bindable<[InputValue, InputHandler<InputValue>]>(
    self => self.input.pipe(
      withLatestFrom(self.inputHandler),
      filter(([value, inputHandler]) => inputHandler.inputChangedFilter(value)),
    ),
    ([value, inputHandler]) => inputHandler.setInput(value)
  )
  private inputChanged!: Bindable<any>;

  @bindable<[Event, InputHandler<InputValue>]>(
    self => fromEvent(self.inputView, self.eventName).pipe(withLatestFrom(self.inputHandler)),
    ([, inputHandler], self) => self.change.next(inputHandler.getInput())
  )
  private inputEventEmitted$!: Bindable<Event>;

  private eventName = 'input';

  constructor(
    private inputView: InputView,
  ) {}
}
