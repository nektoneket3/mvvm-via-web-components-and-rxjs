import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { inject, VIEW } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { OnDestroy } from '~/types';
import { CurrentSubscriptions, View } from '../../types';


type StylesInput = Record<string, Observable<unknown>>;

const { bindable } = getBindableDecorators<StyleDirective>();

@inject(VIEW)
export class StyleDirective implements OnDestroy {

  @input() private style!: Input<StylesInput>;

  @bindable(
    self => self.style,
    (styles, self) => {
      self.subscriptions.add(
        Object.entries(styles).reduce(
          (subscriptions: CurrentSubscriptions, [styleName, bindingValue]: [string, Observable<any>]): CurrentSubscriptions => {
            subscriptions.add(
              bindingValue.pipe(distinctUntilChanged()).subscribe((value: string) => {
                // this.element.setAttribute('style', `${style}:${value}`);
                (self.view.style as any)[styleName] = value;
              })
            );

            return subscriptions;
          },
          new Subscription()
        )
      );
    }
  )
  private styleChanged$!: Bindable<StylesInput>;

  private subscriptions: CurrentSubscriptions = new Subscription();

  constructor(
    private view: View,
  ) {}

  onDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
