import { filter } from 'rxjs/operators';

import { inject, VIEW } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { View } from '../../types';


const { bindable } = getBindableDecorators<ValueDirective>();

@inject(VIEW)
export class ValueDirective {

  @input() private value!: Input<any>;

  @bindable(
    self => self.value.pipe(filter(value => self.view.innerText !== value)),
    (value, self) => self.view.innerText = value
  )
  private valueChanged$!: Bindable<any>;

  constructor(
    private view: View,
  ) {}
}
