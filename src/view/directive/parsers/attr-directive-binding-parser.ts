import { attrBuiltInDirectives } from '../built-in-directives';
import { BindingSource, BindingName } from './types';

import { BindingParser } from './binding-parser';
import { AttrParser } from './attr/attr-parser';
import { ObjectLikeValueAttrParser } from './attr/object-like-value-attr-parser';
import { ObjectPathValueAttrParser } from './attr/object-path-value-attr-parser';

import { InputSourceParser } from './source/source-parser';
import { ObjectLikeSourceParser } from './source/object-like-source-parser';
import { ObjectPathSourceParser } from './source/object-path-source-parser';


export class AttrDirectiveBindingParser extends BindingParser {

  viewContainsDirective(directiveName?: string): boolean {
    if (directiveName) {
      return this.viewHasBindingAttr(directiveName);
    }

    return Object.keys(attrBuiltInDirectives).some((name): boolean => {
      return this.viewHasBindingAttr(name);
    });
  }

  createAttrParser(bindingName: BindingName, attr: Attr): AttrParser {
    switch (bindingName) {
      case 'class':
      case 'style':
        return new ObjectLikeValueAttrParser(attr);
      default:
        return new ObjectPathValueAttrParser(attr);
    }
  }

  createInputSourceParser(bindingName: BindingName, source: BindingSource): InputSourceParser {
    switch (bindingName) {
      case 'class':
      case 'style':
        return new ObjectLikeSourceParser(source);
      default:
        return new ObjectPathSourceParser(source);
    }
  }
}
