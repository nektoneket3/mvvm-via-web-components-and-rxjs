export interface AttrParseResult {
  name: string;
  value: string | string[] | Record<string, string>;
}

export abstract class AttrParser {

  constructor(
    protected attr: Attr
  ) {}

  abstract parse(): AttrParseResult

  protected getValue(): string | never {
    if (!this.attr.value) {
      throw new Error(`${this.attr.name}="" is empty`);
    }

    return this.attr.value;
  }
}
