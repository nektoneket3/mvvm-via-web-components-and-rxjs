import { AttrParser, AttrParseResult } from './attr-parser';
import { ObjectPathValueAttrParser } from './object-path-value-attr-parser';


export interface LoopLikeValueAttrParseResult extends AttrParseResult {
  value: string[];
  isObjectPathLike: boolean;
  objectPathValue?: string[]
}

export class LoopLikeValueAttrParser extends AttrParser {

  private separator: ' in ' = ' in ';

  constructor(
    attr: Attr,
    private objectPathValueAttrParser: ObjectPathValueAttrParser
  ) {
    super(attr);
  }

  getLoopValues(value = this.getValue()): string[] {
    return value.split(this.separator);
  }

  parse(): LoopLikeValueAttrParseResult | never { // TODO refactor it
    if (this.getValue().includes(this.separator) && this.getLoopValues().length === 2) {
      const splitValue = this.getLoopValues();
      const result: LoopLikeValueAttrParseResult = {
        name: this.attr.name,
        value: splitValue,
        isObjectPathLike: false
      };

      if (this.objectPathValueAttrParser.isObjectPathLike(splitValue[1])) {
        result.isObjectPathLike = true;
        result.objectPathValue = this.objectPathValueAttrParser.getObjectPathValue(splitValue[1]);
      }

      return result;
    }

    throw new Error(`\n\nTemplate syntax error.\nValid pattern: item in bindingName`);
  }
}
