import { AttrParser, AttrParseResult } from './attr-parser';


type ParsedParam = [string, string];
type ParsedParams = ParsedParam[];
type Value = Record<string, string>;

export interface ObjectLikeValueAttrParseResult extends AttrParseResult {
  value: Value;
}

export class ObjectLikeValueAttrParser extends AttrParser {

  parse(): ObjectLikeValueAttrParseResult {
    const value = this.getValue();
    const params = this.parseObjectLikeParams(value);

    const result = params.reduce(
      (acc: Value, [className, bindingName]: ParsedParam): Value => {
        // TODO combine all errors and throw error instead of console.error.
        return { ...acc, [className]: bindingName }
      },
      {}
    );

    return { name: this.attr.name, value: result };
  }

  private parseObjectLikeParams(params: string): ParsedParams | never {
    try {
      return Object.entries(JSON.parse(params.replace(/'/g, '"')));
    } catch (error) {
      throw new Error(
        `\n\nTemplate syntax error.\nCurrent value: ${params}.\nValid pattern: { 'name1': 'bindingName1', 'name2': 'bindingName2' }\n`
      );
    }
  }
}
