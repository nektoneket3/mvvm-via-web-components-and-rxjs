import { AttrParser, AttrParseResult } from './attr-parser';


export interface ObjectPathValueAttrParseResult extends AttrParseResult {
  value: string[];
  originalValue: string;
  isStatic?: boolean;
  isReverse?: boolean;
}

type Separator = '.';

export class ObjectPathValueAttrParser extends AttrParser {

  private separator: Separator = '.';

  isObjectPathLike(value = this.getValue()): boolean {
    return value.includes(this.separator) || (value.includes('[') && value.includes(']'));
  }

  getObjectPathValue(value = this.getValue()): string[] {
    if (value.includes(this.separator)) {
      return value.split(this.separator);
    }
    const matches = value.match(/\[(.*?)]/)!;
    return [value.split(matches[0])[0], matches[1]];

  }

  parse(): ObjectPathValueAttrParseResult {
    const result = {
      name: this.attr.name,
      value: this.isObjectPathLike() ? this.getObjectPathValue() : [this.getValue()],
      originalValue: this.getValue(),
      isStatic: this.isStaticValue(),
      isReverse: this.isReverseValue(),
    };

    if (result.isStatic) {
      result.value = [result.originalValue.slice(1,-1)];
    }

    if (result.isReverse) {
      result.value.splice(0, 1, result.value[0].replace('!', ''));
    }

    return result;
  }

  private isStaticValue(value = this.getValue()): boolean {
    return value.startsWith("'") && value.endsWith("'");
  }

  private isReverseValue(value = this.getValue()): boolean {
    return value.startsWith('!');
  }
}
