import { View } from '../../types';
import { AttrParser } from './attr/attr-parser';
import { SourceParser, InputSourceParser, InputSourceParseResult, OutputSourceParseResult } from './source/source-parser';
import { BindingSource, BindingName } from './types';
import { SubjectOrFunctionSourceParser } from '~/view/directive/parsers/source/subject-or-function-source-parser';


export abstract class BindingParser {

  private prefix: string = 'data-bind-';

  constructor(
    private view: View,
  ) {}

  viewContainsDirective(directiveName: string): boolean { // TODO check it
    return this.viewHasBindingAttr(directiveName);
  }

  viewContainsBinding(bindingName: BindingName): boolean { // TODO check it
    return this.viewHasBindingAttr(bindingName);
  }

  getViewBindingAttr(bindingName: BindingName): Attr | null {
    return this.view.attributes.getNamedItem(this.getAttrName(bindingName));
  }

  getInputSource(bindingName: BindingName, source: BindingSource): InputSourceParseResult {
    return this.getSource<InputSourceParseResult>(bindingName, this.createInputSourceParser(bindingName, source));
  }

  getOutputSource(bindingName: BindingName, source: BindingSource): OutputSourceParseResult {
    return this.getSource<OutputSourceParseResult>(bindingName, new SubjectOrFunctionSourceParser(source));
  }

  protected abstract createAttrParser(bindingName: BindingName, attr: Attr): AttrParser;

  protected abstract createInputSourceParser(bindingName: BindingName, source: BindingSource): InputSourceParser;

  protected viewHasBindingAttr(bindingName: BindingName): boolean { // TODO check it
    return !!this.getViewBindingAttr(bindingName);
  }

  private getSource<T>(bindingName: BindingName, sourceParser: SourceParser<any>): T {
    const attrParser: AttrParser = this.createAttrParser(bindingName, this.getViewBindingAttr(bindingName)!);
    return sourceParser.parse(attrParser.parse());
  }

  private getAttrName(name: string): string {
    return `${this.prefix}${name}`;
  }
}
