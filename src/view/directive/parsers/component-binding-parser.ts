import { BindingSource, BindingName } from './types';

import { BindingParser } from './binding-parser';
import { AttrParser } from './attr/attr-parser';

import { InputSourceParser } from './source/source-parser';
import { ObjectPathValueAttrParser } from '~/view/directive/parsers/attr/object-path-value-attr-parser';
import { ObjectPathSourceParser } from '~/view/directive/parsers/source/object-path-source-parser';


export class ComponentBindingParser extends BindingParser {

  createAttrParser(bindingName: BindingName, attr: Attr): AttrParser {
    return new ObjectPathValueAttrParser(attr);
  }

  createInputSourceParser(bindingName: BindingName, source: BindingSource): InputSourceParser {
    return new ObjectPathSourceParser(source);
  }
}
