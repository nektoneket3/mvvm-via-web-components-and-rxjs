import { BindingSource, BindingName } from './types';

import { BindingParser } from './binding-parser';
import { AttrParser } from './attr/attr-parser';
import { LoopLikeValueAttrParser } from './attr/loop-like-value-attr-parser';
import { ObjectPathValueAttrParser } from './attr/object-path-value-attr-parser';

import { InputSourceParser } from './source/source-parser';
import { LoopLikeSourceParser } from './source/loop-like-source-parser';
import { ObjectPathSourceParser } from './source/object-path-source-parser';


export class RepeatDirectiveBindingParser extends BindingParser {

  createAttrParser(bindingName: BindingName, attr: Attr): AttrParser {
    switch (bindingName) {
      case 'repeat':
        return new LoopLikeValueAttrParser(attr, new ObjectPathValueAttrParser(attr));
      default:
        return new ObjectPathValueAttrParser(attr);
    }
  }

  createInputSourceParser(bindingName: BindingName, source: BindingSource): InputSourceParser {
    return new LoopLikeSourceParser(source, new ObjectPathSourceParser(source));
  }
}
