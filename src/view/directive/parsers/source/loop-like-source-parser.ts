import { Observable } from 'rxjs';

import { BindingName, BindingSource } from '../types';
import { InputSourceParser, InputSourceParseResult } from './source-parser';
import { ObjectPathSourceParser } from './object-path-source-parser';
import { LoopLikeValueAttrParseResult } from '../attr/loop-like-value-attr-parser';


export class LoopLikeSourceParser extends InputSourceParser {

  constructor(
    source: BindingSource,
    private objectPathParser: ObjectPathSourceParser
  ) {
    super(source);
  }

  parse({ name, value, isObjectPathLike, objectPathValue }: LoopLikeValueAttrParseResult): InputSourceParseResult {
    const bindingName: BindingName = value[1];

    if (isObjectPathLike && objectPathValue) {
      return this.objectPathParser.parse({ name, value: objectPathValue, originalValue: objectPathValue.join('.') });
    }

    const bindingValue: InputSourceParseResult | void = this.getValue(bindingName);

    if (bindingValue instanceof Observable) {
      return bindingValue;
    }

    throw new Error(`${name}="${bindingName}" is not valid. It must be Observable type, but it is ${typeof bindingValue}.`);
  };
}
