import { Observable, of } from 'rxjs';

import { InputSourceParser, InputSourceParseResult } from './source-parser';
import { ObjectLikeValueAttrParseResult } from '../attr/object-like-value-attr-parser';


export class ObjectLikeSourceParser extends InputSourceParser {

  parse({ name, value }: ObjectLikeValueAttrParseResult): InputSourceParseResult {

    return of(
      Object.entries(value).reduce(
        (acc: Record<string, Observable<any>>, [className, bindingName]: [string, string]): Record<string, Observable<any>> => {

          const bindingValue = this.getValue(bindingName);

          if (bindingValue instanceof Observable) {
            return { ...acc, [className]: bindingValue };
          }

          throw new Error(`${name}="${bindingName}" is not valid. It must be Observable type, but it is ${typeof bindingValue}.`);
        },
        {}
      )
    );
  };
}
