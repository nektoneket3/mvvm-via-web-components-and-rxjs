import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { InputSourceParser, InputSourceParseResult } from './source-parser';
import { ObjectPathValueAttrParseResult } from '../attr/object-path-value-attr-parser';
import { RepeatableIndex } from '../types';


type BindingValue = Observable<any>;

export class ObjectPathSourceParser extends InputSourceParser { // TODO refactor it

  parse({ name, value, originalValue, isStatic, isReverse }: ObjectPathValueAttrParseResult): InputSourceParseResult {
    if (isStatic) {
      return of(value[0]);
    }

    let bindingValue = this.getValue<BindingValue>(value[0]);

    if (!(bindingValue instanceof Observable)) {
      bindingValue = of(bindingValue);
    }

    switch (value.length) {
      case 1:
        break;
      case 2:
        bindingValue = bindingValue.pipe(
          switchMap<Record<string, any | void>, BindingValue>((innerSource) => {
            const repeatableIndex: RepeatableIndex = '$$index';
            const innerBindingValue = value[1] === repeatableIndex
              ? innerSource[this.getValue<number>(repeatableIndex)]
              : innerSource[value[1]];

            if (typeof innerBindingValue === 'undefined') {
              this.throwError(name, originalValue, innerBindingValue);
            }

            return innerBindingValue instanceof Observable ? innerBindingValue : of(innerBindingValue);
          }),
        );
        break;
      default:
        throw new Error('ObjectPathSourceParser: zero or maximum nested objects.');
    }

    return bindingValue.pipe(map(value => isReverse ? !value : value));
  };

  private throwError(bindingName: string, originalValue: string, bindingValue: any): never {
    throw new Error(`${bindingName}="${originalValue}" is not valid. It must be Observable type, but it is ${typeof bindingValue}.`);
  }
}
