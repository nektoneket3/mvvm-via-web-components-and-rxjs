import { Observable, Subject } from 'rxjs';

import { AttrParseResult } from '../attr/attr-parser';
import { ClassInstance } from '~/types';
import { BindingSource, BindingName, Scope } from '../types';


export type InputSourceParseResult = Observable<any> | never;
export type OutputSourceParseResult = Subject<any> | Function | never;

export abstract class SourceParser<T> {

  constructor(
    protected source: BindingSource
  ) {}

  abstract parse(attrParseResult: AttrParseResult): T;

  getValue<R>(bindingName: BindingName): R | never {
    const scopedValue = this.findInScope(this.source.scope || {}, bindingName);

    if (typeof scopedValue !== 'undefined') {
      return scopedValue as R;

    } else if (typeof this.source.component[bindingName] !== 'undefined') {
      return this.source.component[bindingName];
    }

    throw new Error(`${bindingName} is undefined and cannot be used as bindable value.`)
  }

  private findInScope<R>({ scope = {}, parent }: Scope | ClassInstance, bindingName: BindingName): R | void {
    if (scope.hasOwnProperty(bindingName)) {
      return scope[bindingName];
    } else if (parent) {
      return this.findInScope(parent, bindingName);
    }
  }
}

export abstract class InputSourceParser extends SourceParser<InputSourceParseResult> {

  abstract parse(attrParseResult: AttrParseResult): InputSourceParseResult;
}

export abstract class OutputSourceParser extends SourceParser<OutputSourceParseResult> {

  abstract parse(attrParseResult: AttrParseResult): OutputSourceParseResult;
}
