import { Subject } from 'rxjs';

import { OutputSourceParser } from './source-parser';
import { ObjectPathValueAttrParseResult } from '../attr/object-path-value-attr-parser';


type Callback = (value: unknown) => void;
type ParseResult = Subject<unknown> | Callback;

export class SubjectOrFunctionSourceParser extends OutputSourceParser {

  parse({ name, value: v }: ObjectPathValueAttrParseResult): ParseResult {
    let value: string;

    if (typeof v === 'string') {
      value = v;
    } else {
      if (v.length > 1) {
        throw new Error('output binding cannot have "." of "[]" separator')
      }
      value = v[0];
    }

    const bindingValue = this.getValue<ParseResult>(value);

    if (bindingValue instanceof Subject || typeof bindingValue === 'function') {
      return bindingValue;
    }

    throw new Error(`${name}="${value}" is not valid. It must be Subject or Function type, but it is ${typeof bindingValue}.`);
  };
}
