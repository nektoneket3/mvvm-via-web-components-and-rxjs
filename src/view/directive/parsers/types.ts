import { Component } from '~/component';


export type RepeatableIndex = '$$index';

export interface Scope {
  parent?: Scope;
  scope: { [key: string]: unknown } & Partial<Record<RepeatableIndex, number>>;
}

export interface BindingSource {
  scope?: Scope; // need for Repeat directive which has its own scope for each repeatable item
  component: Component;
}
export type BindingName = string;
