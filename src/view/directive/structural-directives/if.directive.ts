import { distinctUntilChanged, map } from 'rxjs/operators';

import { inject, VIEW, VIEW_UPDATED } from '~/di';
import { Input } from '~/component';
import { input, getBindableDecorators, Bindable } from '~/decorator';
import { UpdatedView$, CommentedView, View } from '../../types';


type Condition = boolean;

const { bindable } = getBindableDecorators<IfDirective>();

@inject(VIEW, VIEW_UPDATED)
export class IfDirective{

  @input({ name: 'if' }) private condition!: Input<Condition>;

  @bindable<Condition>(
    self => self.condition.pipe(map(condition => !!condition), distinctUntilChanged()),
    (condition: Condition, self) => {
      self.updatedView.next(condition ? self.getClonedElement() : self.getCommentedElement());
    }
  )
  private conditionChanged!: Bindable<Condition>;

  private readonly elementTemplate: string;

  constructor(
    view: View,
    private updatedView: UpdatedView$,
  ) {
    this.elementTemplate = view.outerHTML;
  }

  private getClonedElement(): HTMLElement {
    const templateElement = document.createElement('template');
    templateElement.innerHTML = this.elementTemplate.trim();
    return templateElement.content.firstChild! as HTMLElement;
  }

  private getCommentedElement(): CommentedView {
    return document.createComment(this.elementTemplate);
  }
}
