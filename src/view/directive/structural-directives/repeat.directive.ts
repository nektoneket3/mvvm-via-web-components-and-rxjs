import { inject, VIEW, VIEW_UPDATED } from '~/di';
import { Input, Output } from '~/component';
import { input, getBindableDecorators, Bindable, output } from '~/decorator';
import { UpdatedView$, View } from '../../types';
import { OnViewConnected } from '~/types';


type RepeatData = any[];

const { bindable } = getBindableDecorators<RepeatDirective>();

@inject(VIEW, VIEW_UPDATED)
export class RepeatDirective implements OnViewConnected {

  @input() private repeat!: Input<RepeatData>;

  @output('repeat-views-connected') onViewConnected!: Output<void>;

  @bindable<RepeatData>(
    self => self.repeat,
    (data: RepeatData, self) => {
      self.updatedView.next(
        data.reduce(
          (acc: View) => {
            acc.appendChild(self.getClonedElement());
            return acc;
          },
          document.createElement('div')
        )
      );
    }
  )
  private repeatChanged!: Bindable<RepeatData>;

  private readonly elementTemplate: string;

  constructor(
    element: HTMLElement,
    private updatedView: UpdatedView$,
  ) {
    this.elementTemplate = element.outerHTML;
  }

  private getClonedElement(): HTMLElement {
    const templateElement = document.createElement('template');
    templateElement.innerHTML = this.elementTemplate.trim();
    return templateElement.content.firstChild! as HTMLElement;
  }
}
