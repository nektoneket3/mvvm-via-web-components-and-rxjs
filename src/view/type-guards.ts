import { View, ViewTemplate, CommentedView } from './types';


export function isView(value: any): value is View {
  return value instanceof HTMLElement;
}

export function isViewTemplate(value: any): value is ViewTemplate {
  return value instanceof DocumentFragment;
}

export function isComment(value: any): value is CommentedView {
  return value instanceof Comment;
}
