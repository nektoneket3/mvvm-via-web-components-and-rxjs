import { Observable, ReplaySubject, Subscription } from 'rxjs';

import { ClassCtor, ClassInstance, OnDestroy, OnInit } from '~/types';


export type View = HTMLElement;
export type View$ = Observable<HTMLElement>;
export type CommentedView = Comment;
export type CommentedView$ = Observable<CommentedView>;
export type PossibleView = View | CommentedView;
export type PossibleView$ = Observable<PossibleView>;
export type UpdatedView$ = ReplaySubject<PossibleView>;
export type ViewTemplate = DocumentFragment;

export type DirectiveName = string;
export type AttrName = string;
export type CurrentSubscriptions = Subscription;
export type DirectiveCtor = ClassCtor<Directive>;
export type Directive = ClassInstance & Partial<OnInit> & Partial<OnDestroy>;
export type DirectiveCtors = Record<DirectiveName, DirectiveCtor>;
export type DirectiveCtorFactory = (view: View) => DirectiveCtor;
export type DirectiveCtorOrFactory = { factory: DirectiveCtorFactory } | DirectiveCtor;
export type DirectiveCtorFactories = Record<string, DirectiveCtorOrFactory>
