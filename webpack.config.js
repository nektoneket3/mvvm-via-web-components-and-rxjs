const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const packageJson = require('./package');


const paths = {
  entryPoint: path.resolve(__dirname, 'src/index.ts'),
  bundles: path.resolve(__dirname, 'npm/dist/bundles'),
};

module.exports = {
  mode: 'production',
  entry: {
    'index.umd': [paths.entryPoint],
    'index.umd.min': [paths.entryPoint]
  },
  output: {
    path: paths.bundles,
    filename: '[name].js',
    library: packageJson.name,
    libraryTarget: 'umd'
  },
  externals: [/^rxjs\//],
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [/node_modules/, /test-app/],
        use: {
          loader: 'ts-loader'
        }
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '~': path.resolve(__dirname, 'src'),
    },
  },
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
        include: /\.min\.js$/,
      })
    ],
  },
};
